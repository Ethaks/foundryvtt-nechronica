# Nechronica for FoundryVTT

This game system for [Foundry Virtual Tabletop](http://foundryvtt.com/) provides character sheet and game system support for **Nechronica -The Long Long Sequel-**.

## Installation

To install the system navigate to Foundry's *Game Systems* tab in the Setup menu and paste the following link in the **Install System** dialog:

[https://gitlab.com/Ethaks/foundryvtt-nechronica/-/raw/master/system.json](https://gitlab.com/Ethaks/foundryvtt-nechronica/-/raw/master/system.json)

The system can also be installed manually by downloading a zip archive from the Releases Page and extracting it to the `Data/systems/nechronica` directory.

## Building

Manually building the game system is possible as well, and recommended if contributing to the software is desired.
This can be done by following these steps:

```bash
# Clone the repository
$ git clone https://gitlab.com/Ethaks/foundryvtt-nechronica.git

# Install development dependencies
$ npm install

# Start a file watcher to build and copy the system to the dist directory
$ npm run build:watch

# Copy or link the dist directory to Foundry's Data/systems directory as "nechronica"
```

## Contributing

Merge requests and issues are welcome.
For major changes, please open an issue beforehand.

## Legal

The software component of this system is distributed under the EUPL v. 1.2, while the game content is distributed under the rules regarding the creation of derivative works as per the system's [website](http://www.nechronica.com/download.html).
The terms of the [Foundry Virtual Tabletop End User License Agreement](https://foundryvtt.com/article/license/) apply.
This game system is not affiliated to or endorsed by Ryou Kamiya or Incog Lab.
