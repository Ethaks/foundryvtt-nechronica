# Changelog

## 0.1.2

### Bug Fixes

- The "Edit Item" button in character sheets after adding an item could open the sheet of the item previously on that row.
- The contents of the last location in character sheets disappeared in an area that could not be reached by scrolling.

## 0.1.1

### Bug Fixes

- Initial actor data preparation produced errors due to combatants and their AP not being available

## 0.1.0

- The first official release!
