import { NechronicaActorDataSource } from "./module/actor/actor-data";
import NechronicaActor from "./module/actor/actor-nech";
import { NechronicaActorDataProperties } from "./module/actor/actor-prepared-data";
import NechronicaActorSheetDoll from "./module/actor/doll-sheet";
import NechronicaActorSheetHorror from "./module/actor/horror-sheet";
import NechronicaActorSheetLegion from "./module/actor/legion-sheet";
import NechronicaActorSheetSavant from "./module/actor/savant-sheet";
import NechronicaCombat, {
  CombatantTurnHistoryEntry,
  CombatRoundHistoryEntry,
} from "./module/combat/combat";
import { NechronicaCombatant } from "./module/combat/combatant";
import { NECHRONICA } from "./module/config";
import NechronicaDice from "./module/dice";
import { NechronicaItemData } from "./module/item/item-data";
import NechronicaItem from "./module/item/item-nech";
import NechronicaItemSheet from "./module/item/item-sheet";
import * as macros from "./module/macros";

export {};
declare global {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  interface Array<T> {
    deepEquals(other: unknown): boolean;
  }

  interface Game {
    /**
     * A namespace for Nechronica-specific exports. Classes and functions in
     * here constitute the system's API to be used by macros and modules.
     */
    nechronica: {
      NechronicaActor: typeof NechronicaActor;
      NechronicaItem: typeof NechronicaItem;
      NechronicaDice: typeof NechronicaDice;
      NechronicaCombatant: typeof NechronicaCombatant;
      macros: typeof macros;
      rollItemMacro: typeof macros.rollItemMacro;
      spineCounterMacro: typeof macros.spineCounterMacro;
      sheets: {
        NechronicaActorSheetDoll: typeof NechronicaActorSheetDoll;
        NechronicaActorSheetHorror: typeof NechronicaActorSheetHorror;
        NechronicaActorSheetLegion: typeof NechronicaActorSheetLegion;
        NechronicaActorSheetSavant: typeof NechronicaActorSheetSavant;
        NechronicaItemSheet: typeof NechronicaItemSheet;
      };
    };

    /** Object added by Dice So Nice */
    dice3d?: {
      enabled: boolean;
    };
  }

  interface CONFIG {
    /**
     * Configuration values and objects used by the system, as well as translation
     * strings for various types. Localisation strings are expected to be
     * translated during the system's loading procedure.
     *
     * Changes to dictionaries contained herein will affect the system's runtime
     * behaviour, e.g. by adding additional item subtypes or actions.
     */
    NECHRONICA: typeof NECHRONICA;
  }

  interface DocumentClassConfig {
    Actor: typeof NechronicaActor;
    Item: typeof NechronicaItem;
    Combat: typeof NechronicaCombat;
    Combatant: typeof NechronicaCombatant;
  }
  interface SourceConfig {
    Actor: NechronicaActorDataSource;
    Item: NechronicaItemData;
  }
  interface DataConfig {
    Actor: NechronicaActorDataProperties;
    Item: NechronicaItemData;
  }
  interface FlagConfig {
    Combat: {
      nechronica?: {
        roundHistory?: CombatRoundHistoryEntry[];
      };
    };
    Combatant: {
      nechronica?: {
        spineCounter?: number;
        initiativePenalty?: number;
        history?: CombatantTurnHistoryEntry[];
      };
    };
  }
}
