import { NECHRONICA } from "../config";

export type NechronicaActorDataSource =
  | NechronicaDollDataSource
  | NechronicaSavantDataSource
  | NechronicaHorrorDataSource
  | NechronicaLegionDataSource;

export type ActorType = "doll" | "savant" | "horror" | "legion";

export interface NechronicaActorDataHelper<D, T extends ActorType> {
  type: T;
  data: D;
}

interface NechronicaDollDataSource {
  type: "doll";
  data: NechronicaDollDataSourceData;
}
interface NechronicaSavantDataSource {
  type: "savant";
  data: NechronicaSavantDataSourceData;
}
interface NechronicaHorrorDataSource {
  type: "horror";
  data: NechronicaHorrorDataSourceData;
}
interface NechronicaLegionDataSource {
  type: "legion";
  data: NechronicaLegionDataSourceData;
}

export interface NechronicaActorDataSourceDataAP {
  value: number;
  bonus: number | null;
  current: number;
  spineCounter: number;
  max: number;
}

export interface NechronicaActorDataSourceDataAttribute {
  bonus: number;
  experience: number;
}

export type Attribute = keyof typeof NECHRONICA.attributes;

export type NechronicaActorDataDataAttributes = Record<
  Attribute | string,
  NechronicaActorDataSourceDataAttribute
>;

export interface NechronicaActorDataSourceDataDetails {
  notes: string;
}

export interface NechronicaDollDataSourceDataDetails extends NechronicaActorDataSourceDataDetails {
  biography: string;
  age: string;
  experience: number;
  premonitions: string;
}

interface NechronicaActorDataSourceDataBase {
  ap: NechronicaActorDataSourceDataAP;
}

export interface NechronicaDollDataSourceData extends NechronicaActorDataSourceDataBase {
  class: string;
  subClass: string;
  position: string;
  placement: Placement;
  details: NechronicaDollDataSourceDataDetails;
  attributes: NechronicaActorDataDataAttributes;
}

export type Placement = keyof typeof NECHRONICA.placements;

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface NechronicaSavantDataSourceData extends NechronicaActorDataSourceDataBase {}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface NechronicaHorrorDataSourceData extends NechronicaActorDataSourceDataBase {}

interface NechronicaLegionDataSourceData extends NechronicaActorDataSourceDataBase {
  remaining: number;
}
