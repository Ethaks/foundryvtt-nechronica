import NechronicaActorSheet from "./actor-sheet";
import { NECHRONICA } from "../config";

export default class NechronicaActorSheetSavant extends NechronicaActorSheet {
  /** @override */
  static get defaultOptions(): BaseEntitySheet.Options {
    // @ts-expect-error mergeObject types
    return mergeObject(super.defaultOptions, {
      classes: [...super.defaultOptions.classes, "savant"],
    });
  }

  /**
   * Handle dropping items onto a Savan't sheet
   *
   * @param event - The drop event
   * @param data - The item data
   * @returns False if no location was chosen, drop result otherwise
   */
  /* eslint-disable @typescript-eslint/no-explicit-any */
  /* eslint-disable @typescript-eslint/explicit-module-boundary-types */
  async _onDropItem(event: DragEvent, data: any) {
    // Check for skills
    if (!this.actor.owner) return false;
    const item = await Item.fromDropData(data);
    const itemData = duplicate(item?.data);

    if (itemData.type === "bodyPart" && itemData.data.type === "skill") {
      const setLocation = (itemData: any, location: string) => {
        setProperty(itemData, "data.type", "bodyPart");
        setProperty(itemData, "data.location", location);
        return true;
      };
      // @TODO: cleanup
      const dialog = await new Promise((resolve) => {
        new Dialog({
          title: game.i18n.format("NECH.ChooseLocationForSkill", {
            name: data.name,
          }),
          content: "",
          buttons: {
            head: {
              label: game.i18n.localize(NECHRONICA.locations.head),
              callback: () => resolve(setLocation(itemData, "head")),
            },
            arms: {
              label: game.i18n.localize(NECHRONICA.locations.arms),
              callback: () => resolve(setLocation(itemData, "arms")),
            },
            torso: {
              label: game.i18n.localize(NECHRONICA.locations.torso),
              callback: () => resolve(setLocation(itemData, "torso")),
            },
            legs: {
              label: game.i18n.localize(NECHRONICA.locations.legs),
              callback: () => resolve(setLocation(itemData, "legs")),
            },
          },
          close: () => {
            resolve(false);
          },
          default: "head",
        }).render(true);
      });
      // @ts-expect-error ItemData casting leads to incompatibility
      if (dialog) return this._onDropItemCreate(itemData);
      else return false;
    }
    super._onDropItem(event, data);
  }
}
