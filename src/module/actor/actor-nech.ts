import { SpendApOptions } from "../combat/combat";
import { Location, Modifier, NechronicaItemData } from "../item/item-data";
import NechronicaItem from "../item/item-nech";
import { NECHRONICA } from "../config";
import { NechronicaActorRollData, SourceDetail } from "./actor-prepared-data";
import { getGame, localize } from "../lib/utils";
import { Attribute, NechronicaActorDataSource } from "./actor-data";
import { ActorDataConstructorData } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/actorData";
import { DropFirst, PropPath, PropType } from "../common/common-data";
import { Document } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/abstract/module.mjs";
import { NechronicaCombatant } from "../combat/combatant";

/**
 * The NechronicaActor Document which represents PCs and NPCs
 */
export default class NechronicaActor extends Actor {
  public optionalModifiers: { name: string; modifier: Modifier }[] | undefined;
  public sourceDetails: Record<string, SourceDetail[]> | undefined;

  /**
   * A convenience getter for the actor's combatant, if any
   */
  get combatant(): NechronicaCombatant | undefined {
    // Return early if combats are undefined, since the game is still getting initialised
    if (getGame().combats == null) return undefined;
    const combat = getGame()?.combat;
    // Return when there is no combat, hence no combatant
    if (!combat) return;

    if (this.isToken) {
      const tokenId = this.token?.id;
      if (tokenId) return combat?.getCombatantByToken(tokenId);
    } else {
      const tokens = this.getActiveTokens()?.filter(
        (t) => t.data.actorLink === true
      ) as unknown as Token[]; // Error in types, this returns Token[] not TokenDocument[]
      let combatant: NechronicaCombatant | undefined;
      tokens
        ?.filter((t) => t.scene.id === combat?.data.scene)
        .find((t) => {
          if (t.id) {
            combatant = combat?.getCombatantByToken(t.id);
            return !!combatant;
          }
        });
      return combatant;
    }
  }

  /**
   * The number of AP belonging to the actor's current combatant if any
   */
  get ap(): number | null {
    return this.combatant?.initiative ?? null;
  }

  /**
   * The number of AP currently in the actor's current combatant's spine counter if any
   */
  get spineCounter(): number {
    return this.combatant?.data.flags.nechronica?.spineCounter ?? 0;
  }

  /**
   * Spend AP from this actor's combatant's AP pool if there is a current combatant
   *
   * @param ap - Number of AP to subtract
   * @param options - Optional parameters
   * @returns Number of AP spent from pool and spine counter
   */
  async spendAp(
    ap: number,
    options: SpendApOptions = {}
  ): Promise<void | { apSpent: number; usedSpineCounter: number }> {
    const combatant = this.combatant;
    // Actually spend AP when the user owns this actor and is thus allowed to update the combatant
    if (combatant && this.isOwner) {
      return this.combatant?.spendAp(ap, options);
    }
  }

  /** @override */
  prepareBaseData(): void {
    const actorData = this.data;

    // Threat base values
    if (actorData.type !== "doll") {
      if (actorData.type === "savant") actorData.data.threat = -8;
      if (actorData.type === "horror") actorData.data.threat = -3.5;
    }
  }

  /** @override */
  override getRollData(): NechronicaActorRollData {
    const rollData = { ...foundry.utils.deepClone(this.data.data), actorType: this.data.type };
    return rollData as NechronicaActorRollData;
  }

  /** @override */
  override prepareDerivedData(): void {
    const actorData = this.data;
    const data = actorData.data;

    // Doll specific data
    if (actorData.type === "doll") {
      // Generate attribute (experience) bonus
      for (const attribute of Object.keys(NECHRONICA.attributes)) {
        const curAttribute = actorData.data.attributes[attribute];
        actorData.data.attributes[attribute].total = curAttribute.bonus + curAttribute.experience;
      }
    }

    // General actor data

    // Reset derived instance data
    this.optionalModifiers = [];
    this.sourceDetails = {};

    // Base value for max AP
    data.ap.max = 6 + (data.ap.bonus ?? 0);
    this.addSourceDetail("data.ap.max", [
      {
        name: localize("Base"),
        bonus: 6,
      },
    ]);
    if (data.ap.bonus && data.ap.bonus > 0) {
      this.addSourceDetail("data.ap.max", {
        name: localize("APBonus"),
        bonus: data.ap.bonus,
      });
    }

    // Initialise basic general modifiers
    data.modifiers = {};
    Object.keys(NECHRONICA.attackTypes).forEach(
      (key) => (data.modifiers[key] = { damage: 0, attack: 0 })
    );
    // Initialise parts object to hold tooltips and HP bars, populate with static and placeholder data
    setProperty(data, "parts.all", { value: 0, max: 0 });
    // Add skills at top of part list
    if (actorData.type === "doll") {
      data.parts.skills = {
        header: { label: localize("Skills"), max: 0, value: 0 },
        value: 0,
        max: 0,
      };
    }
    // Either add locations or all part collection
    if (["doll", "savant"].includes(actorData.type)) {
      Object.entries(NECHRONICA.locations).forEach(([key, value]) => {
        data.parts[key] = {
          header: {
            label: value,
            value: 0,
            max: 0,
          },
          value: 0,
          max: 0,
        };
      });
    } else {
      actorData.data.parts.all.header = {
        label: localize("Parts"),
        max: 0,
        value: 0,
      };
    }

    // Item dependent data
    for (const item of this.items) {
      const itemData: NechronicaItemData = item.data;
      // Doll specific data
      if (actorData.type === "doll") {
        // Generate attribute totals
        if (itemData.type === "class") {
          for (const attribute of Object.keys(NECHRONICA.attributes) as Attribute[]) {
            actorData.data.attributes[attribute].total += itemData.data[attribute];
          }
        }
      }

      // Body Parts
      if (itemData.type === "bodyPart") {
        // Threat calculation
        if (actorData.type !== "doll") {
          if (itemData.data.threat) {
            actorData.data.threat += itemData.data.threat;
          }
        }

        // Parts summary for tokens
        if (itemData.type === "bodyPart") {
          // Location the part will be listed in, according to parts list type
          let location;
          if (!["doll", "savant"].includes(actorData.type)) location = "all";
          else if (actorData.type === "doll" && itemData.data.type === "skill") location = "skills";
          else location = itemData.data.location;

          if (!location) continue;

          const locData = data.parts[location] ?? {};
          // Maximum number of parts (for location and total)
          if (location !== "all") data.parts.all.max += 1;
          locData.max += 1;
          // Increase counter of intact parts, save item data for tooltip rendering
          if (!(item as NechronicaItem).isBroken) {
            if (location !== "all") data.parts.all.value += 1;
            locData.value += 1;
            locData.data = [...(locData.data ?? []), item.data.toObject(false)];
          }
          data.parts[location] = locData;
        }

        // Benefits granted by intact parts
        if (!(item as NechronicaItem).isBroken) {
          // Add max AP
          const apBonus = "apBonus" in item.data.data ? item.data.data.apBonus : null;
          if (apBonus) {
            data.ap.max += apBonus;
            this.addSourceDetail("data.ap.max", {
              name: item.data.name,
              bonus: apBonus,
            });
          }

          // General modifiers
          const modifiers = itemData.data.modifiers ?? [];
          for (const modifier of modifiers) {
            if (modifier.optional === "0") {
              // Generate key string
              const dest = `${modifier.target}.${modifier.type}`;
              const oldBonus: number = getProperty(data.modifiers, dest);
              if (modifier.formula.length > 0) {
                const cleanFormula = Roll.parse(modifier.formula, data)
                  .filter((t) => t.isDeterministic)
                  .map((t) => t.total)
                  .join("");
                const bonus = Roll.safeEval(cleanFormula);

                // Save increased modifier
                setProperty(data.modifiers, dest, oldBonus + bonus);
                // Save source details
                this.addSourceDetail(`data.modifiers.${dest}`, {
                  name: modifier.tag || item.name || "",
                  bonus,
                });
              }
            } else if (modifier.optional === "1") {
              // Cache optional modifiers on actor
              this.optionalModifiers.push({
                name: modifier.tag || item.name || "",
                modifier,
              });
            }
          }
        }
      }
    }

    // Create tooltips from intact parts
    const tooltipPromises = [];
    // Begin with common tooltip header
    tooltipPromises.push(
      renderTemplate("/systems/nechronica/templates/tooltip-header.hbs", actorData)
    );
    for (const [key, loc] of Object.entries(actorData.data.parts)) {
      // Skip integer fields for part counts
      if (["max", "value"].includes(key)) continue;
      // Skip all (parts count) for actors with actual locations
      if (["doll", "savant"].includes(this.data.type) && key === "all") {
        continue;
      }

      // Data used to render tooltip template, header according to list type
      const templateData = {
        config: NECHRONICA,
        isLegion: false,
        header: mergeObject(loc.header ?? {}, {
          label: "",
          value: loc.value ?? 0,
          max: loc.max ?? 0,
          icon: NECHRONICA.tooltipIcons[key as keyof typeof NECHRONICA["tooltipIcons"]] ?? {
            icon: "",
            color: "",
          },
        }),
        // Remove p tag from effects to enable display on same line
        list: loc.data
          ?.sort((a, b) => (a.sort || 0) - (b.sort || 0))
          .map((d) => {
            let effectString = TextEditor.enrichHTML(d.data.effect ?? "", {
              rollData: d,
            });
            effectString = effectString.match(/(?:<p>)(.*)(?:<\/?p>)/)?.[1] ?? effectString ?? "";
            d.data.effect = effectString;
            return d;
          }),
      };
      // Add legion specific data to header
      if (actorData.type === "legion") {
        setProperty(templateData, "header.remaining", actorData.data.remaining);
        templateData.isLegion = true;
      }
      // Render template and save result in actor data (.then to leave function signature intact)
      tooltipPromises.push(
        renderTemplate("/systems/nechronica/templates/tooltip.hbs", templateData)
      );
      delete loc.data;
      delete loc.header;
    }
    // Resolve promises and create one HTML element for presentation
    Promise.all(tooltipPromises).then((t) => {
      data.tooltip = t.join("");
    });

    this.onApUpdate();
  }

  /**
   * A helper method setting this actor's AP and Spine Counter to its combatant's values, if any.
   * Also re-draws each token's bars so they match the actor.
   * TODO: This could lag behind if scenes/combats are switched.
   */
  onApUpdate(): void {
    const data = this.data.data;
    data.spineCounter.max = data.ap.max;
    data.spineCounter.value = this.spineCounter;
    data.ap.value = this.ap ?? 0;
    this.getActiveTokens().forEach((token) => token.drawBars());
  }

  /**
   * @override
   */
  async update<P extends PropPath<NechronicaActorDataSource>>(
    data:
      | DeepPartial<ActorDataConstructorData | NechronicaActorDataSource>
      | Record<P, PropType<NechronicaActorDataSource, P>>
      | { bonusAttribute?: Attribute | "null" },
    options = {}
  ): Promise<this | undefined> {
    // Handle attribute bonus
    if ("bonusAttribute" in data && data.bonusAttribute !== "null" && this.data.type === "doll") {
      // Reset bonus attribute
      for (const key of Object.keys(this.data.data.attributes)) {
        setProperty(data, `data.attributes.${key}.bonus`, 0);
      }

      // Set bonus attribute
      foundry.utils.setProperty(data, `data.attributes.${data.bonusAttribute}.bonus`, 1);
    } else if ("bonusAttribute" in data && data.bonusAttribute === "null") {
      delete data.bonusAttribute;
    }

    return super.update(data, options);
  }

  /**
   * Adds details containing a source and a numeric bonus to the actor's source details
   *
   * @param key - The data key affected by this source
   * @param values - A single SourceDetail or an array of SourceDetails to be added to the key
   */
  addSourceDetail(key: string, values: SourceDetail | SourceDetail[]): void {
    if (!Array.isArray(values)) values = [values];
    this.sourceDetails = this.sourceDetails ?? {};
    if (this.sourceDetails[key] === undefined) {
      this.sourceDetails[key] = [...values];
    } else this.sourceDetails[key].push(...values);
  }

  /**
   * Change the "Broken" state of one or multiple items, or a whole location
   *
   * @param targets - Indicator for items to change
   * @param state - State that is to be set
   * @returns The updated Documents
   */
  async setItemBroken(
    target: SetItemTargets = {},
    state: 0 | 1 | "toggle" = 1
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  ): Promise<Document<any, this>[] | void> {
    const location = target.location;
    let paramItems = target.paramItems;
    // States: used true, unused false, "toggle"
    if (!paramItems?.length && location === "") return;

    if (paramItems != null && !Array.isArray(paramItems)) {
      paramItems = [paramItems];
    }
    let items: NechronicaItem[] = [];
    if (paramItems?.length) {
      items = paramItems.flatMap((i) => {
        const item = this.items.get(i);
        return item ? [item] : [];
      });
    } else if (location != null && location.length > 0) {
      const locationItems = this.items.filter(
        (i) =>
          i.data.type === "bodyPart" &&
          i.data.data.type === "bodyPart" &&
          (i.data.data.location === location || location === "all")
      );
      items.push(...locationItems);
    } else return [];

    const updates = items.reduce(
      (arr: { _id: string; "data.broken": boolean }[], item: NechronicaItem) => {
        const oldState = foundry.utils.getProperty(item.data, "data.broken");
        if (state !== "toggle" && state === oldState) return arr;
        const newState = state === "toggle" ? !!(oldState ^ 1) : !!state;
        if (item.id) arr.push({ _id: item.id, "data.broken": newState });
        return arr;
      },
      []
    );

    if (!updates.length) return [];

    return this.updateEmbeddedDocuments("Item", updates);
  }

  /**
   * Change the "Used" state of one or multiple items, or a whole location
   *
   * @param paramItems - One or multiple IDs
   * @param location - A location string, or "all" for all parts, or "skills" for all skills
   * @param state - State that is to be set
   * @returns The updated Document
   */
  async setItemUsed(
    target: SetItemTargets = {},
    state: 0 | 1 | "toggle" = 1
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  ): Promise<Document<any, this>[] | void> {
    let paramItems = target.paramItems;
    const location = target.location;
    // States: used true, unused false, "toggle"
    if (!paramItems?.length && location === "") return;

    if (paramItems != null && !Array.isArray(paramItems)) {
      paramItems = [paramItems];
    }
    let items: NechronicaItem[] = [];
    if (paramItems?.length) {
      items = paramItems.flatMap((i) => {
        const item = this.items.get(i);
        return item ? [item] : [];
      });
    } else if (location != null && location.length > 0) {
      const locationItems = this.items.filter(
        (i) =>
          i.data.type === "bodyPart" &&
          !i.isRepeatable &&
          (location === "skills"
            ? i.data.data.type === "skill"
            : i.data.data.type === "bodyPart" &&
              (location === "all" || i.data.data.location === location))
      );
      items.push(...locationItems);
    } else return [];

    const updates = items.reduce((arr, item) => {
      const oldState = getProperty(item.data, "data.used");
      if (state !== "toggle" && state === oldState) return arr;
      const newState = state === "toggle" ? !!(oldState ^ 1) : !!state;
      // @ts-expect-error newState is a boolean since only 0 or 1 are possible
      if (item.id) arr.push({ _id: item.id, "data.used": newState });
      return arr;
    }, []);

    if (!updates.length) return [];

    return this.updateEmbeddedDocuments("Item", updates);
  }

  // The following comment block to silence linter and compiler is atrocious…
  /** @override */
  /* eslint-disable @typescript-eslint/explicit-module-boundary-types */
  override _onUpdateEmbeddedDocuments(
    embeddedName: string,
    ...args: DropFirst<Parameters<Actor["_onUpdateEmbeddedDocuments"]>>
  ): void {
    super._onUpdateEmbeddedDocuments(embeddedName, ...args);
    /* eslint-enable @typescript-eslint/explicit-module-boundary-types */

    // Ensure bar redraw to display accurate part counts
    if (embeddedName === "Item") {
      this.getActiveTokens().forEach((token) => token.drawBars());
    }
  }

  override _onUpdate(...args: Parameters<Actor["_onUpdate"]>): void {
    super._onUpdate(...args);
    this.getActiveTokens().forEach((t) => t.drawBars());
  }
}

/**
 * An object containing targets whose status is to be set or toggled
 */
interface SetItemTargets {
  /**
   * A single item ID or an array of item IDs
   */
  paramItems?: string | string[];
  /**
   * A shorthand string to allow setting or toggling all parts of a location
   */
  location?: Location | "all" | "skills" | "";
}
