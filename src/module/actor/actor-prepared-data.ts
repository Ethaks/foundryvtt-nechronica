import { ItemDataSource } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/itemData";
import { Location } from "../item/item-data";
import {
  ActorType,
  Attribute,
  NechronicaActorDataSourceDataAP,
  NechronicaActorDataSourceDataAttribute,
  NechronicaActorDataHelper,
  NechronicaDollDataSourceData,
} from "./actor-data";
import NechronicaActorSheetV2 from "./actor-sheet-v2";

export type NechronicaActorDataProperties =
  | NechronicaDollDataProperties
  | NechronicaSavantDataProperties
  | NechronicaHorrorDataProperties
  | NechronicaLegionDataProperties;

export type NechronicaDollDataProperties = NechronicaActorDataHelper<
  NechronicaDollDataPropertiesData,
  "doll"
>;
export type NechronicaSavantDataProperties = NechronicaActorDataHelper<
  NechronicaSavantDataPropertiesData,
  "savant"
>;
export type NechronicaHorrorDataProperties = NechronicaActorDataHelper<
  NechronicaHorrorDataPropertiesData,
  "horror"
>;
export type NechronicaLegionDataProperties = NechronicaActorDataHelper<
  NechronicaLegionDataPropertiesData,
  "legion"
>;

type NechronicaActorRollDataHelper<
  T extends NechronicaActorDataProperties["data"],
  U extends ActorType
> = Record<string, unknown> & T & { actorType: U };

export type NechronicaDollRollData = NechronicaActorRollDataHelper<
  NechronicaDollDataPropertiesData,
  "doll"
>;
export type NechronicaSavantRollData = NechronicaActorRollDataHelper<
  NechronicaSavantDataPropertiesData,
  "savant"
>;
export type NechronicaHorrorRollData = NechronicaActorRollDataHelper<
  NechronicaHorrorDataPropertiesData,
  "horror"
>;
export type NechronicaLegionRollData = NechronicaActorRollDataHelper<
  NechronicaLegionDataPropertiesData,
  "legion"
>;

export type NechronicaActorRollData =
  | NechronicaDollRollData
  | NechronicaSavantRollData
  | NechronicaHorrorRollData
  | NechronicaLegionRollData;

interface NechronicaActorDataPropertiesBase {
  ap: NechronicaActorDataPropertiesDataAp;
  spineCounter: { max: number; value: number };
  tooltip: string;

  modifiers: Record<string, { damage: number; attack: number }>;
  parts: Record<Location | "all" | "skills" | string, Parts>;
}

interface Parts {
  header?: {
    label: string;
    max: number;
    value: number;
    icon?: { icon: string; color: string };
    remaining?: number;
  };
  max: number;
  value: number;
  data?: ItemDataSource[];
}

interface NechronicaActorDataPropertiesDataAp extends NechronicaActorDataSourceDataAP {
  max: number;
}

export interface NechronicaActorDataPropertiesDataAttribute
  extends NechronicaActorDataSourceDataAttribute {
  total: number;
}

type NechronicaActorDataPropertiesDataAttributes = Record<
  Attribute | string,
  NechronicaActorDataPropertiesDataAttribute
>;

interface NechronicaDollDataPropertiesData
  extends NechronicaActorDataPropertiesBase,
    NechronicaDollDataSourceData {
  attributes: NechronicaActorDataPropertiesDataAttributes;
  sourceDetails: Record<string, SourceDetail[]>;
}

interface NechronicaActorDataPropertiesData {
  threat: number;
}

export interface NechronicaSavantDataPropertiesData
  extends NechronicaActorDataPropertiesBase,
    NechronicaActorDataPropertiesData {}

export interface NechronicaLegionDataPropertiesData
  extends NechronicaActorDataPropertiesBase,
    NechronicaActorDataPropertiesData {
  remaining: number;
}

export interface NechronicaHorrorDataPropertiesData
  extends NechronicaActorDataPropertiesBase,
    NechronicaActorDataPropertiesData {}

/*
 * An object countaining a single bonus and the name of its source
 */
export interface SourceDetail {
  /*
   * The name of the bonus source
   */
  name: string;
  /*
   * The bonus added by the source
   */
  bonus: number;
}

export type NechronicaActorSheetData = ReturnType<NechronicaActorSheetV2["getData"]>;
