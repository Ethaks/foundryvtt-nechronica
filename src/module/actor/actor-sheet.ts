/* eslint-disable */
// @ts-nocheck
import { ItemType, Location } from "../item/item-data";
import NechronicaItem from "../item/item-nech";
import { addCircles } from "../lib/utils";
import { NECHRONICA } from "../config";
import NechronicaDice from "../dice";
import NechronicaActor from "./actor-nech";

/** @augments ActorSheet */
export default class NechronicaActorSheet extends ActorSheet {
  private _hiddenElems: Record<string, any> = {};
  static get defaultOptions() {
    // @ts-expect-error mergeObject types
    return mergeObject(super.defaultOptions, {
      classes: ["nech", "sheet", "actor"],
      width: 700,
      height: 800,
      scrollY: [".sheet-body", ".bodyParts-all", ".parts-body"],
    });
  }

  // @ts-ignore args silliness
  constructor(...args) {
    // @ts-ignore args silliness
    super(...args);

    // Track display status of elements to persist after re-rendering
    this._hiddenElems = {};
  }

  /** @override */
  get template() {
    if (!game.user?.isGM && this.actor.limited) {
      return `systems/nechronica/templates/actors/limited.hbs`;
    }
    return `systems/nechronica/templates/actors/${this.actorType}.hbs`;
  }

  /**
   * A convenience reference to the actor type
   *
   * @returns {string} The actor's type
   */
  get actorType(): string {
    return this.actor.data.type;
  }

  /** @override */
  getData() {
    const data = {
      isOwner: this.entity.owner,
      limited: this.entity.limited,
      options: this.options,
      editable: this.isEditable,
      cssClass: this.entity.owner ? "editable" : "locked",
      rollData: this.actor.getRollData(),
      isGM: game.user?.isGM ?? false,
      actor: duplicate(this.actor.data),
      config: CONFIG.NECHRONICA,
    };

    const actorData = data.actor;
    actorData.items = actorData.items.sort((a, b) => (a.sort || 0) - (b.sort || 0));

    // Add type specific data
    if (this.actorType === "doll") {
      this.addDollData(actorData);
    } else if (this.actorType === "savant") {
      this.addSavantData(actorData);
    } else if (this.actorType === "legion") {
      this.addLegionData(actorData);
    } else if (this.actorType === "horror") {
      this.addHorrorData(actorData);
    }

    actorData.ap = {};
    actorData.ap.items =
      actorData.items.reduce((total, current) => {
        if (current.type === "bodyPart") total += current.data.apBonus;
        return total;
      }, 0) ?? 0;

    data.hiddenElems = this._hiddenElems;

    actorData.data.ap.value = this.actor.ap ?? 0;
    actorData.data.spineCounter.value = this.actor.spineCounter ?? 0;

    return data;
  }

  /**
   * Add data specific to Doll-type actors
   *
   * @param {object} data - The data to be returned by getData
   */
  addDollData(data) {
    this._sortItemsLocations(data);
  }

  /**
   * Add data specific to Savant-type actors
   *
   * @param {object} data - The data to be returned by getData
   */
  addSavantData(data) {
    this._sortItemsLocations(data);
  }

  /**
   * Add data specific to Horror-type actors
   *
   * @param {object} data - The data to be returned by getData
   */
  addHorrorData(data) {
    this._sortItemsSimple(data);
  }

  /**
   * Add data specific to Legion-type actors
   *
   * @param {object} data - The data to be returned by the sheet's getData
   */
  addLegionData(data) {
    this._sortItemsSimple(data);
  }

  /** @override */
  activateListeners(html: JQuery<HTMLElement>) {
    super.activateListeners(html);

    // Owned Item management
    html.find(".item-add").click((ev) => this._onItemCreate(ev));
    html.find(".item-edit").click(this._onItemEdit.bind(this));
    html.find(".item-delete").click(this._onItemDelete.bind(this));
    html.find(".item-duplicate").click(this._duplicateItem.bind(this));

    // Open item with rightclick
    html.find(".item .name, .target h4").contextmenu(this._onItemEdit.bind(this));

    // Edit items directly from sheet
    html.find(".item-quick-edit").change((ev) => this._onItemQuickEdit(ev));

    // Item actions
    html.find(".item-action").click(this._onItemAction.bind(this));

    // Roll items to chat with left click
    html.find(".item .name, .item .target").click(this._onItemAction.bind(this));

    // Header rollables
    html.find(".location-header>.rollable").click(this._onHeaderRoll.bind(this));

    // Handle location usage
    html.find(".use-location").click((ev) => this._onLocationUse(ev));
    //
    // Handle location breaking
    html.find(".break-location").click((ev) => this._onLocationBreak(ev));

    // Handle hiding the header
    html.find("a.hide-show").click(this._hideShowHeader.bind(this));

    // Enable field selection after render
    if (this.isEditable) {
      const inputs = html.find("input");
      inputs.focus((ev) => ev.currentTarget.select());
    }
  }

  /**
   * Toggle the display of the doll header
   *
   * @param {Event} event - The triggering click event
   * @private
   */
  _hideShowHeader(event) {
    event.preventDefault();
    const html = this._element as JQuery<HTMLElement>;
    const sheetHeader = html.find(".sheet-header");
    const header = html.find(".npc-header");
    const sheetBody = html.find(".sheet-body");
    const img = html.find(".sheet-header>.flexrow>img");

    if (header.hasClass("hidden")) {
      header.removeClass("hidden");

      sheetBody.removeClass("small-header");
      img.removeClass("small-img");
      sheetHeader.removeClass("small-sheet-header");

      this._hiddenElems["npc-header"] = false;
    } else {
      header.addClass("hidden");
      sheetBody.addClass("small-header");
      img.addClass("small-img");
      sheetHeader.addClass("small-sheet-header");

      this._hiddenElems["npc-header"] = true;
    }
  }

  /**
   * Rolls a header/location
   *
   * @param {Event} event - The event triggering the roll
   * @returns {ChatMessage} The chat message generated by the roll
   */
  _onHeaderRoll(event): Promise<ChatMessage | null | undefined> {
    event.preventDefault();
    const li = $(event.currentTarget).closest(".location")[0];
    const data = { ...li.dataset };
    data.actor = this.actor;
    data.title =
      data.type === "bodyPart"
        ? NECHRONICA.locations[data.location as Location]
        : game.i18n.localize("NECH.Roll.Roll");
    data.image = `systems/nechronica/icons/parts_${data.location}.png`;
    data.type = game.i18n.localize("NECH.Roll.Dismemberment");

    return NechronicaDice.itemRoll(data);
  }

  /**
   * Change the "Broken" state of all Parts (belonging to a location)
   *
   * @async
   * @param {Event} event - The triggering click event
   * @returns {Promise<Entity|Entity[]>}   The updated Entity or array of Entities
   */
  async _onLocationBreak(event) {
    event.preventDefault();
    const li = $(event.currentTarget).closest(".header")[0];
    const location = li.dataset.location;
    const state = game.keyboard?.isDown("Shift")
      ? 1
      : game.keyboard?.isDown("Control")
      ? "toggle"
      : 0;

    const actor = this.actor as NechronicaActor;
    return actor.setItemBroken({ location }, state);
  }

  /**
   * Change the "Used" state of all Parts (belonging to a location) or all Skills
   *
   * @async
   * @param {Event} event - The triggering click event
   * @returns {Promise<Entity|Entity[]>}   The updated Entity or array of Entities
   */
  async _onLocationUse(event) {
    event.preventDefault();
    const li = $(event.currentTarget).closest(".header")[0];
    const location = li.dataset.location;
    const state = game.keyboard?.isDown("Shift")
      ? 1
      : game.keyboard?.isDown("Control")
      ? "toggle"
      : 0;

    const actor = this.actor as NechronicaActor;
    return actor.setItemUsed({ location }, state);
  }

  /**
   * Sorts Item entities by their location
   *
   * @param {object} data - The data to be returned by the sheet's getData
   */
  _sortItemsLocations(data) {
    // Create general bodyParts object and categories
    data.bodyParts = {};
    for (const category of ["fetters", "memories", "attacks", "skills"]) {
      data[`${category}`] = [];
    }

    // Generate locations to hold body parts
    for (const [k, v] of Object.entries(NECHRONICA.locations)) {
      data.bodyParts[k] = { label: v, parts: [] };
    }

    // Populate item categories
    for (const item of data.items) {
      if (item.type === "bodyPart") {
        item.showUsed = !["action", "auto"].includes(item.data.timing);
        // Skills don't need a location
        if (item.data.type === "skill") {
          // @TODO: Handle skills for savants?
          data.skills.push(item);
        } else {
          // regular body parts do
          data.bodyParts[item.data.location].parts = [
            ...(data.bodyParts[item.data.location ?? "head"].parts ?? []),
            item,
          ];
        }
      } else if (item.type === "attack") {
        data.attacks.push(item);
      } else if (item.type === "fetter") {
        addCircles(item, 4, "data.madness");
        data.fetters.push(item);
      } else if (item.type === "memory") {
        data.memories.push(item);
      } else if (item.type === "class") {
        if (this.actor.data.data.class === item._id) data.class = item;
        else if (this.actor.data.data.subClass === item._id) {
          data.subClass = item;
        }
      } else if (item.type === "position") {
        if (this.actor.data.data.position === item._id) data.position = item;
      }
    }
  }

  // eslint-disable-next-line
  _sortItemsSimple(data) {
    data.bodyParts = data.items
      .filter((i) => i.type === "bodyPart")
      .map((i) => {
        i.showUsed = !["action", "auto"].includes(i.data.timing);
        return i;
      });
  }

  /**
   * Duplicates an Item on the sheet's actor
   *
   * @async
   * @param {Event} event       The triggering click event
   * @returns {Promise<Item>}   The duplicated item
   */
  async _duplicateItem(event) {
    event.preventDefault();
    const a = event.currentTarget;

    const itemId = $(a).parents(".item").attr("data-item-id");
    if (itemId == null) return;
    const item = this.actor.getOwnedItem(itemId);
    const data = duplicate(item.data);

    // @ts-ignore The ID is necessary for Items, but not for the creation data
    delete data._id;
    data.name = game.i18n.format("NECH.CopyName", { name: data.name });

    return this.actor.createEmbeddedEntity("OwnedItem", data);
  }

  /**
   * Handle requests to update an OwnedItem's data from the owner's sheet
   *
   * @param {Event} event - The triggering event
   */
  _onItemQuickEdit(event) {
    event.preventDefault();
    const id = $(event.currentTarget).parents(".item").attr("data-item-id");
    const target = $(event.currentTarget).attr("data-target");
    if (id == null || target == null) return;
    const item = duplicate(this.actor.getEmbeddedEntity("OwnedItem", id));
    let value = event.target.value;
    if (event.currentTarget.type === "checkbox") {
      value = !getProperty(item, target);
    }
    setProperty(item, target, value);
    this.actor.updateEmbeddedEntity("OwnedItem", item);
  }

  /**
   * Handle requests to create a new OwnedItem
   *
   * @param {Event} event - The triggering event
   * @returns {object} The created OwnedItem
   */
  _onItemCreate(event) {
    event.preventDefault();
    const header = $(event.currentTarget);
    const type = header.attr("data-type") as ItemType;
    const subType = header.attr("data-sub-type");
    const typeName = subType ?? type;
    const name = game.i18n.format("NECH.NewItem", {
      name: game.i18n.localize(NECHRONICA.itemTypes[typeName as ItemType]),
    });
    const itemData = <any>{
      // This is dirty. Type checking please!
      name,
      type,
      data: {},
      img: DEFAULT_TOKEN,
    };
    if (type === "bodyPart") {
      if (subType === "skill") itemData.data.type = "skill";
      else {
        const location = header.attr("data-location") ?? "";
        itemData.data.location = location;
        if (location && ["doll", "savant"].includes(this.actor.data.type)) {
          itemData.img = `/systems/nechronica/icons/parts_${location}.png`;
        }
      }
    }
    return this.actor.createEmbeddedEntity("OwnedItem", itemData);
  }

  /**
   * Handle requests to open an OwnedItems's sheet
   *
   * @param {Event} event - The triggering event
   */
  _onItemEdit(event) {
    event.preventDefault();
    const li = event.currentTarget.closest(".item");
    const item = this.actor.getOwnedItem(li.dataset.itemId);
    item.sheet?.render(true);
  }

  /**
   * Handle requests to delete an OwnedItem
   *
   * @param {event} event - The triggering event
   */
  _onItemDelete(event) {
    event.preventDefault();

    const button = event.currentTarget;
    if (button.disabled) return;

    const li = event.currentTarget.closest(".item");
    if (keyboard?.isDown("Shift")) {
      this.actor.deleteOwnedItem(li.dataset.itemId);
    } else {
      button.disabled = true;

      const item = this.actor.items.find((o) => o._id === li.dataset.itemId);
      if (item == null) return;
      const msg = `<p>${game.i18n.localize("NECH.DeleteItemConfirmation")}</p>`;
      Dialog.confirm({
        title: game.i18n.format("NECH.DeleteItemTitle", { item: item.name }),
        content: msg,
        yes: () => {
          this.actor.deleteOwnedItem(li.dataset.itemId);
          button.disabled = false;
        },
        no: () => (button.disabled = false),
      });
    }
  }

  /**
   * Roll an item to chat
   *
   * @param {Event} event - The click event triggering the action
   */
  _onItemAction(event) {
    event.preventDefault();
    const a = event.currentTarget;
    const item = this.actor.getOwnedItem(a.closest(".item").dataset.itemId) as NechronicaItem;

    // Check rolls
    if (a.classList.contains("check")) {
      item.roll({ rollType: RollType.CHECK, ev: event });
    }

    // Attack rolls
    if (a.classList.contains("attack")) {
      item.roll({ rollType: RollType.ATTACK, ev: event });
    }

    // Roll to chat
    if (
      a.classList.contains("chat") ||
      a.classList.contains("name") ||
      a.classList.contains("target")
    ) {
      item.roll({ rollType: RollType.MESSAGE, ev: event });
    }
  }
}
