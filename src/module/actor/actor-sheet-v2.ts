import type { NechronicaItemData, PartType } from "../item/item-data";
import { ItemType, Location } from "../item/item-data";
import { NECHRONICA, RollType } from "../config";
import NechronicaDice, { NechronicaRollCardData } from "../dice";
import NechronicaActor from "./actor-nech";
import NechronicaActorSheetBase from "../../app/NechronicaActorSheetBase.svelte";
import { ActorType } from "./actor-data";
import { NechronicaActorDataProperties, NechronicaActorRollData } from "./actor-prepared-data";

// Svelte
import type { SvelteComponent } from "svelte";
import { writable, Writable } from "svelte/store";
import { getGame, localize } from "../lib/utils";

// Foundry types
import { Document } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/abstract/module.mjs";
import { AnyDocumentData } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/abstract/data.mjs";
import { ActorData } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/module.mjs";

export default class NechronicaActorSheetV2 extends ActorSheet<
  ActorSheet.Options,
  NechronicaActorSheetData
> {
  constructor(actor: NechronicaActor, options: ActorSheet.Options) {
    super(actor, options);

    // Track display status of elements to persist after re-rendering
    this._hiddenElems = {
      header: false,
    };
  }

  static get defaultOptions(): ActorSheet.Options {
    return mergeObject(super.defaultOptions, {
      classes: ["nech", "sheet", "actor"],
      width: 700,
      height: 800,
      scrollY: [".sheet-body", ".bodyParts-all", ".parts-body"],
    });
  }

  /**
   * The rendered SvelteComponent belonging to this sheet
   */
  private app: SvelteComponent | null = null;

  /**
   * The store used to make data available to the SvelteComponent {@link this.app}
   */
  private dataStore: Writable<NechronicaActorSheetData> | null = null;

  /**
   * A Record of elements to be hidden,
   * stored on the class instance to persist after closing the sheet
   */
  public _hiddenElems: Record<string, boolean> = {};

  /** @override */
  override get template(): string {
    if (!getGame().user?.isGM && this.actor.limited) {
      return "systems/nechronica/templates/actors/limited.hbs";
    }
    return "systems/nechronica/templates/actors/sheet.hbs";
  }

  /**
   * A convenience reference to the actor type
   */
  get actorType(): ActorType {
    return this.actor.data.type as ActorType;
  }

  /** @override */
  override render(force = false, options: Application.RenderOptions = {}): this {
    // Register the active Application with the referenced Documents
    this.object.apps[this.appId] = this;

    // Render Svelte application
    this._renderSvelte(force, options);

    return this;
  }

  /** @override */
  override close(options?: Application.CloseOptions): Promise<void> {
    if (this.app != null) {
      this.app.$destroy();
      this.app = null;
      this.dataStore = null;
    }
    return super.close(options);
  }
  /** @override */
  override async getData(): Promise<NechronicaActorSheetData> {
    const actorData = this.actor.data.toObject(false);
    const context = await super.getData();
    actorData.items = actorData.items.sort((a, b) => (a.sort || 0) - (b.sort || 0));

    const apData = {
      items:
        actorData.items.reduce((total, current) => {
          if (current.type === "bodyPart") total += current.data.apBonus;
          return total;
        }, 0) ?? 0,
      value: this.actor.ap ?? 0,
    };

    const spineCounterData = {
      value: this.actor.spineCounter ?? 0,
    };

    return {
      ...context,

      isOwner: this.actor.isOwner,
      owner: this.object.isOwner,
      document: this.object,
      limited: this.actor.limited,
      options: this.options,
      editable: this.isEditable,
      cssClass: this.actor.isOwner ? "editable" : "locked",
      rollData: this.actor.getRollData(),
      isGM: getGame().user?.isGM ?? false,
      actor: this.object,
      config: NECHRONICA,
      sheet: this,
      hiddenElems: this._hiddenElems,
      actorData: { ...actorData, ap: apData, spineCounter: spineCounterData },
      data: actorData.data,
    };
  }
  /*
   * Renders Foundry's regular application first, and then renders the Svelte sheet into that application
   *
   * @param {boolean} force     Render and display the application even if it is not currently displayed.
   * @param {Object} options    New Application options which update the current values of the Application#options object
   */
  private async _renderSvelte(
    force = false,
    options: Application.RenderOptions = {}
  ): Promise<void> {
    const data = await this.getData();
    if (this.app !== null) {
      this.dataStore?.set(data);
      return;
    } else {
      try {
        await this._render(force, options);
      } catch (err) {
        if (err instanceof Error) {
          err.message = `An error occurred while rendering ${this.constructor.name} ${this.appId}: ${err.message}`;
        }
        console.error(err);
        this._state = Application.RENDER_STATES.ERROR;
      }

      if (isObjectEmpty(this.element)) return;
      const targetElement = this.element.find("form").get(0);
      if (!targetElement) return;
      this.dataStore = writable(data);
      this.app = new NechronicaActorSheetBase({
        target: targetElement,
        props: {
          dataStore: this.dataStore,
        },
      });
    }
    return;
  }

  /**
   * Handle dropping items onto a Savan't sheet
   *
   * @param event - The drop event
   * @param data - The item data
   * @returns False if no location was chosen, drop result otherwise
   */
  override async _onDropItem(event: DragEvent, data: ActorSheet.DropData.Item): Promise<unknown> {
    if (this.actor.data.type === "savant") {
      // Check for skills
      if (!this.actor.isOwner) return false;
      const item = await Item.fromDropData(data);
      if (!item) return;
      const itemData = item?.data.toObject();

      if (itemData.type === "bodyPart" && itemData.data.type === "skill") {
        const setLocation = (itemData: any, location: string) => {
          setProperty(itemData, "data.type", "bodyPart");
          setProperty(itemData, "data.location", location);
          return true;
        };
        // @TODO: cleanup
        const dialog = await new Promise((resolve) => {
          new Dialog({
            title: localize("ChooseLocationForSkill", {
              name: item.name,
            }),
            content: "",
            buttons: {
              head: {
                label: NECHRONICA.locations.head,
                callback: () => resolve(setLocation(itemData, "head")),
              },
              arms: {
                label: NECHRONICA.locations.arms,
                callback: () => resolve(setLocation(itemData, "arms")),
              },
              torso: {
                label: NECHRONICA.locations.torso,
                callback: () => resolve(setLocation(itemData, "torso")),
              },
              legs: {
                label: NECHRONICA.locations.legs,
                callback: () => resolve(setLocation(itemData, "legs")),
              },
            },
            close: () => {
              resolve(false);
            },
            default: "head",
          }).render(true);
        });
        if (dialog) return this._onDropItemCreate(itemData);
        else return false;
      }
    }
    return super._onDropItem(event, data);
  }
  /* eslint-enable @typescript-eslint/no-explicit-any */
  /* eslint-enable @typescript-eslint/explicit-module-boundary-types */

  /**
   * Rolls a header/location
   */
  async _onHeaderRoll(event: MouseEvent): Promise<ChatMessage | null | undefined> {
    event.preventDefault();
    const li = $(event.currentTarget as HTMLElement).closest(".location")[0];
    const data = { ...li.dataset } as unknown as Partial<NechronicaRollCardData> & {
      location: Location | "skills";
    };
    if (data.location === "skills") return;
    data.actor = this.actor as NechronicaActor;
    data.title =
      data.type === "bodyPart" ? NECHRONICA.locations[data.location] : localize("Roll.Roll");
    data.image = `systems/nechronica/icons/parts_${data.location}.png`;
    data.type = localize("Roll.Dismemberment");

    return NechronicaDice.itemRoll(data);
  }

  /**
   * Change the "Broken" state of all Parts (belonging to a location)
   */
  async _onLocationBreak(event: MouseEvent): ReturnType<NechronicaActor["setItemBroken"]> {
    event.preventDefault();
    const li = $(event.currentTarget as HTMLElement).closest(".location")[0];
    const location = li.dataset.location as Location | "skills" | "all" | undefined;
    if (location == null) return;
    const state = isKeyDown("SHIFT") ? 1 : isKeyDown("CTRL") ? "toggle" : 0;

    const actor = this.actor as NechronicaActor;
    return actor.setItemBroken({ location }, state);
  }

  /**
   * Change the "Used" state of all Parts (belonging to a location) or all Skills
   */
  async _onLocationUse(event: MouseEvent): ReturnType<NechronicaActor["setItemUsed"]> {
    event.preventDefault();
    const li = $(event.currentTarget as HTMLElement).closest(".location")[0];
    const location = li.dataset.location as Location | "skills" | "all" | undefined;
    if (location == null) return;
    const state = isKeyDown("SHIFT") ? 1 : isKeyDown("CTRL") ? "toggle" : 0;

    const actor = this.actor as NechronicaActor;
    return actor.setItemUsed({ location }, state);
  }

  /**
   * Duplicates an Item on the sheet's actor
   */
  async _duplicateItem(event: MouseEvent): Promise<Document<ActorData, NechronicaActor>[] | void> {
    event.preventDefault();
    const a = event.currentTarget as HTMLElement;

    const itemId = $(a).parents(".item").attr("data-item-id");
    if (itemId == null) return;
    const item = this.actor.items.get(itemId);
    if (!item) return;
    const data = item.data.toObject();

    // @ts-expect-error The ID is necessary for Items, but not for the creation data
    delete data._id;
    data.name = localize("CopyName", { name: data.name });

    // @ts-expect-error Creating items from item data objects is indded possible, types are wrong
    return this.actor.createEmbeddedDocuments("Item", [data]);
  }

  /**
   * Handle requests to update an OwnedItem's data from the owner's sheet
   */
  _onItemQuickEdit(event: MouseEvent): void {
    event.preventDefault();
    const element = event.currentTarget as HTMLInputElement;
    const id = $(element).parents(".item").attr("data-item-id");
    const target = $(element).attr("data-target");
    if (id == null || target == null) return;
    const item = this.actor.items.get(id);
    if (item == null) return;
    // @ts-expect-error TODO jQuery type checking
    let value = event.target.value;
    if (element.type === "checkbox") {
      value = !getProperty(item.data, target);
    }
    this.actor.updateEmbeddedDocuments("Item", [{ _id: item.id, [target]: value }]);
  }

  /**
   * Handle requests to create a new OwnedItem
   */
  _onItemCreate({
    type = "bodyPart",
    subType = "bodyPart",
    location = "head",
  }: {
    type: ItemType;
    subType: PartType;
    location: Location;
  }): Promise<Document<AnyDocumentData, this["object"]>[]> {
    const typeName = subType ? NECHRONICA.partTypes[subType] : NECHRONICA.itemTypes[type];
    const name = localize("NewItem", {
      name: typeName,
    });
    const itemData: DeepPartial<NechronicaItemData> & { name: string } = {
      name,
      type,
      data: {
        description: "",
        effect: "",
      },
    };

    if (itemData.type === "bodyPart" && itemData.data) {
      itemData.data.type = subType;
      if (itemData.data.type === "bodyPart") itemData.data.location = location;
    }

    return this.actor.createEmbeddedDocuments("Item", [itemData]);
  }

  /**
   * Handle requests to open an OwnedItems's sheet
   */
  _onItemEdit(id: string): void {
    const item = this.actor.items.get(id);
    item?.sheet?.render(true);
  }

  /**
   * Handle requests to delete an OwnedItem
   */
  _onItemDelete(event: MouseEvent): void {
    event.preventDefault();

    const button = event.currentTarget as HTMLLinkElement;
    if (button?.disabled) return;

    const li = button.closest(".item") as HTMLUListElement;
    if (li.dataset.itemId == null) return;
    if (isKeyDown("SHIFT")) {
      this.actor.deleteEmbeddedDocuments("Item", [li.dataset.itemId]);
    } else {
      button.disabled = true;

      const item = this.actor.items.find((o) => o.id === li.dataset.itemId);
      if (item == null) return;
      const msg = `<p>${localize("DeleteItemConfirmation")}</p>`;
      Dialog.confirm({
        title: localize("DeleteItemTitle", { item: item.name }),
        content: msg,
        yes: () => {
          if (li.dataset.itemId) this.actor.deleteEmbeddedDocuments("Item", [li.dataset.itemId]);
          button.disabled = false;
        },
        no: () => (button.disabled = false),
      });
    }
  }

  /**
   * Roll an item to chat
   */
  _onItemAction(event: MouseEvent): void {
    event.preventDefault();
    const a = event.currentTarget as HTMLLinkElement;
    const itemId = (a.closest(".item") as HTMLUListElement).dataset.itemId;
    if (itemId == null) return;
    const item = this.actor.items.get(itemId);
    if (item == null) return;

    // Check rolls
    if (a.classList.contains("check")) {
      item.roll({ rollType: RollType.CHECK });
    }

    // Attack rolls
    if (a.classList.contains("attack")) {
      item.roll({ rollType: RollType.ATTACK });
    }

    // Roll to chat
    if (
      a.classList.contains("chat") ||
      a.classList.contains("name") ||
      a.classList.contains("target")
    ) {
      item.roll({ rollType: RollType.MESSAGE });
    }
  }

  _onCircleClick(
    event: MouseEvent
  ): Promise<Document<AnyDocumentData, this["object"]>[] | this["object"] | undefined> | void {
    const actorData = duplicate(this.actor);
    const currentTarget = $(event.currentTarget as HTMLInputElement);
    const index = Number(currentTarget.attr("data-index"));
    let target = currentTarget.parents(".circle-row").attr("data-target");
    if (target == null) return;
    if (target === "item") {
      const itemData = duplicate(
        this.actor.items.find((i) => i.id === currentTarget.parents(".item").attr("data-item-id"))
      );
      target = currentTarget.parents(".circle-row").attr("data-item-target");
      // @ts-expect-error TODO circle type
      const value = getProperty(itemData, target);
      if (value === index + 1) {
        // If the last one was clicked, decrease by 1
        // @ts-expect-error TODO circle type
        setProperty(itemData, target, index);
      } // Otherwise, value = index clicked
      // @ts-expect-error TODO circle type
      else setProperty(itemData, target, index + 1);
      return this.actor.updateEmbeddedDocuments("Item", [itemData]);
    }
    const value = getProperty(actorData, target);
    if (value === index + 1) {
      // If the last one was clicked, decrease by 1
      setProperty(actorData, target, index);
    } // Otherwise, value = index clicked
    else setProperty(actorData, target, index + 1);
    return this.actor.update(actorData);
  }
}

// TODO: Remove as part of keybinding rework
/** Returns whether a key is currently down */
function isKeyDown(key: string) {
  const MOD_KEYS = {
    SHIFT: ["ShiftLeft", "ShiftRight"],
    CTRL: ["ControlLeft", "ControlRight"],
    ALT: ["AltLeft", "AltRight"],
  } as const;
  if (key in MOD_KEYS)
    return MOD_KEYS[key as keyof typeof MOD_KEYS].some((k) => getGame().keyboard?.downKeys.has(k));
  else return getGame().keyboard?.downKeys.has(key);
}

interface NechronicaActorSheetData extends Omit<ActorSheet.Data, "data"> {
  actor: NechronicaActor;
  actorData: NechronicaActorSheetActorData;
  config: typeof NECHRONICA;
  cssClass: string;
  data: NechronicaActorSheetActorData["data"];
  editable: boolean;
  hiddenElems: Record<string, boolean>;
  isGM: boolean;
  isOwner: boolean;
  limited: boolean;
  options: ActorSheet.Options;
  rollData: NechronicaActorRollData;
  sheet: NechronicaActorSheetV2;
}

type NechronicaActorSheetActorData = NechronicaActorDataProperties & {
  data: { ap: { value: number }; spineCounter: { value: number } };
  ap: { items?: number };
  spineCounter: { value: number };
};
