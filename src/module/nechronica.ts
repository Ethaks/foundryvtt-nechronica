import NechronicaActor from "./actor/actor-nech";
import NechronicaActorSheetDoll from "./actor/doll-sheet";
import NechronicaActorSheetHorror from "./actor/horror-sheet";
import NechronicaActorSheetLegion from "./actor/legion-sheet";
import NechronicaActorSheetSavant from "./actor/savant-sheet";
import NechronicaCombat from "./combat/combat";
import NechronicaCombatTracker from "./combat/combatTracker";
import { registerHandlebarsHelpers } from "./handlebars/helpers";
import { preloadHandlebarsTemplates } from "./handlebars/templates";
import NechronicaItem from "./item/item-nech";
import NechronicaItemSheet from "./item/item-sheet";
import { getGame, getUser, initHelpers } from "./lib/utils";
import * as chat from "./chat";
import { NECHRONICA } from "./config";
import NechronicaDice from "./dice";
import * as macros from "./macros";
import { initPatches } from "./patch-core";
import NechronicaActorSheetV2 from "./actor/actor-sheet-v2";
import { MergeObjectOptions } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/utils/helpers.mjs";
import { NechronicaCombatant } from "./combat/combatant";

Hooks.once("init", () => {
  console.log(`Nechronica | Initializing Nechronica Game System\n${NECHRONICA.ASCII}`);

  // Register system specific classes
  CONFIG.Actor.documentClass = NechronicaActor;
  CONFIG.Item.documentClass = NechronicaItem;
  CONFIG.Combat.documentClass = NechronicaCombat;
  CONFIG.Combatant.documentClass = NechronicaCombatant;
  CONFIG.ui.combat = NechronicaCombatTracker;

  // Make system classes available – game or global?
  getGame().nechronica = {
    NechronicaActor,
    NechronicaItem,
    NechronicaDice,
    NechronicaCombatant,
    macros,
    rollItemMacro: macros.rollItemMacro,
    spineCounterMacro: macros.spineCounterMacro,
    sheets: {
      NechronicaActorSheetDoll,
      NechronicaActorSheetHorror,
      NechronicaActorSheetLegion,
      NechronicaActorSheetSavant,
      NechronicaItemSheet,
    },
  };

  // Load config
  CONFIG.NECHRONICA = NECHRONICA;

  preloadHandlebarsTemplates();
  registerHandlebarsHelpers();

  // Initialise general helpers
  initHelpers();

  // Patch core
  initPatches();

  // Register custom ItemSheet
  Items.unregisterSheet("core", ItemSheet);
  // @ts-expect-error Unnecessarily restricting types
  Items.registerSheet("nechronica", NechronicaItemSheet, { makeDefault: true });

  // Register custom ActorSheet
  Actors.unregisterSheet("core", ActorSheet);
  // Svelte actor sheet
  // @ts-expect-error Unnecessarily restricting types do not allow for differing data definitions
  Actors.registerSheet("nechronica", NechronicaActorSheetV2, {
    types: ["doll", "legion", "savant", "horror"],
    label: "NECH.ActorSheet",
    makeDefault: false,
  });
});

Hooks.on("setup", () => {
  const toLocalize = [
    "itemTypes",
    "locations",
    "timings",
    "attackTypes",
    "modifierTypes",
    "effectTypes",
    "partTypes",
    "placements",
    "attributes",
    "hitLocations",
  ];
  const doLocalize = function (obj: Record<string, string>) {
    return Object.entries(obj).reduce(
      (obj: Record<string, string | unknown>, e: [string, string]) => {
        if (typeof e[1] === "string") obj[e[0]] = getGame().i18n.localize(e[1]);
        else if (typeof e[1] === "object") obj[e[0]] = doLocalize(e[1]);
        return obj;
      },
      {}
    );
  };
  for (const o of toLocalize) {
    // @ts-expect-error Only iterating over properties and re-insterting; TODO there's probably a better way using Object.fromEntries?
    NECHRONICA[o] = doLocalize(NECHRONICA[o]);
  }
});

Hooks.on("ready", () => {
  Hooks.on("hotbarDrop", (_bar, data, slot) => macros.createNechronicaMacro(data, slot));

  // Re-run AP detection for all actors when their combatants can actually be accessed
  getGame().actors?.forEach((a) => a.onApUpdate());
});

Hooks.on("preCreateItem", (item: NechronicaItem) => {
  const actor = item.parent;
  if (actor == null) return;
  // Prevent adding classes to actors who already have a class and a subClass
  if (item.data.type === "class" && actor.data.type === "doll") {
    // Allow handling of classes if their ID is already saved on the actor (happens on initial Token updates)
    if (item.id === actor.data.data.class || item.id === actor.data.data.subClass) {
      return true;
    } else if (actor.data.data.class && actor.data.data.subClass) return false;
  }
  // Prevent adding a position to actors who already have a position
  if (
    item.type === "position" &&
    actor.data.type === "doll" &&
    actor.data.data.position &&
    actor.data.data.position !== item.id
  ) {
    return false;
  }
});

Hooks.on("createItem", (item: NechronicaItem, _: MergeObjectOptions, userId: string) => {
  if (!item.id) return;
  const actor = item.parent;
  // Narrow type to Actor
  if (!(actor instanceof Actor)) return;

  // Only continue for the user triggering the item creation
  if (userId !== getGame().user?.id) return;

  // Save class id as primary or sub class
  if (
    actor.data.type === "doll" &&
    item.data.type === "class" &&
    ![actor.data.data.class, actor.data.data.subClass].includes(item.id)
  ) {
    if (actor.data.data.class) actor.update({ "data.subClass": item.id });
    else actor.update({ "data.class": item.id });
  }

  // Save position id as poasition
  if (item.data.type === "position") {
    actor.update({ "data.position": item.id });
  }
});

Hooks.on("deleteItem", (item: NechronicaItem, _: MergeObjectOptions, userId: string) => {
  if (!item.id) return;
  const actor = item.parent;
  if (!(actor instanceof Actor)) return;

  // Only continue for the user triggering the item deletion
  if (userId !== getGame().user?.id) return;

  // Clean up saved (sub)class id
  if (item.data.type === "class" && actor.data.type === "doll") {
    if (actor.data.data.class === item.id) {
      actor.update({
        "data.class": "",
      });
    } else if (actor.data.data.subClass === item.id) {
      actor.update({
        "data.subClass": "",
      });
    }
  }

  // Clean up saved position id
  if (
    actor.data.type === "doll" &&
    item.data.type === "position" &&
    actor.data.data.position === item.id
  ) {
    actor.update({ "data.position": "" });
  }
});

Hooks.on(
  "updateItem",
  (
    item: NechronicaItem,
    data: DeepPartial<typeof item["data"]>,
    _options: MergeObjectOptions,
    userId: string
  ) => {
    const actor = item.parent;
    if (!(actor instanceof Actor) || !item.id) return;

    // Create chat message upon part breaking, or let first GM add broken part to last message
    if (
      item.data.type === "bodyPart" &&
      !!data.data &&
      "broken" in data.data &&
      data?.data?.broken
    ) {
      if (!getUser().isGM || !actor.testUserPermission(getUser(), "OWNER")) return;
      chat.notifyItemBreakage(actor, item, userId);
    }

    // Only continue for the user triggering the item creation
    if (userId !== getUser().id) return;

    // Roll fetter upon gaining 4 madness
    if (
      item.data.type === "fetter" &&
      !!data.data &&
      "madness" in data.data &&
      data.data?.madness === 4
    ) {
      item?.roll();
    }
  }
);

Hooks.on("preCreateActor", (actor: NechronicaActor) => {
  // Set token defaults for newly created actors
  if (!actor.token) {
    actor.data.update({
      "token.displayName": CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER, // Default display name to be on owner hover
      "token.displayBars": CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER, // Default display bars to be on owner hover
      "token.disposition": CONST.TOKEN_DISPOSITIONS.HOSTILE, // Default disposition to hostile
      "token.name": actor.data.name, // Set token name to actor name
    });
  }
  if (actor.data.type === "doll") {
    // Enable token vision, actor links, and default disposition for PCs
    actor.data.update({
      "token.vision": true,
      "token.actorLink": true,
      "token.disposition": CONST.TOKEN_DISPOSITIONS.FRIENDLY,
    });
  }
});

Hooks.on("updateCombatant", (combatant: NechronicaCombatant) => {
  // Set AP and SpineCounter values, trigger bar draws
  combatant.token?.actor?.onApUpdate();
});

Hooks.on("renderChatMessage", (app, html) => {
  chat.highlightCriticals(app, html);
});

Hooks.on("renderChatLog", (_: Application, html: JQuery<HTMLElement>) =>
  chat.activateListeners(html)
);
