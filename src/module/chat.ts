import NechronicaActor from "./actor/actor-nech";
import NechronicaItem from "./item/item-nech";
import { getGame, getUser } from "./lib/utils";

/**
 * Attaches classes to HTML dice roll elements, allowing proper highlighting to be applied.
 *
 * @param message - A single ChatMessage to check for criticals
 * @param html - A JQuery element for the chat message's content
 */
export const highlightCriticals = function (message: ChatMessage, html: JQuery<HTMLElement>): void {
  // Handle attack or check roll
  const roll = message.data.flags.roll as Roll;
  if (roll == null || !("total" in roll) || roll.total == null) return;
  if (roll?.total > 10) {
    html.find(".nech-roll .dice-total").addClass("critical");
  } else if (roll?.total > 1 && roll?.total < 6) {
    html.find(".nech-roll .dice-total").addClass("failure");
  } else if (roll?.total >= 6 && roll?.total < 11) {
    html.find(".nech-roll .dice-total").addClass("success");
  } else if (roll?.total < 2) {
    html.find(".nech-roll .dice-total").addClass("fumble");
  }
};

/*
 * A record containing a cache of item break notifications, to be handled by a debounced
 * function clearing the cache after a delay to enable proper notification collecting.
 */
const breakNotifications: Record<
  string,
  { actor: NechronicaActor; items: NechronicaItem[]; userId: string }
> = {};

/**
 * Creates a chat message describing an item being broken, or adds
 * the item to the most recent message if it already contained this actor's items being broken
 *
 * @param actor - Actor owning the item
 * @param item - Item being broken
 * @param userId - The ID of the user triggering the event
 * @returns The ChatMessage being created or updated
 */
export const notifyItemBreakage = async function (
  actor: NechronicaActor,
  item: NechronicaItem,
  userId: string
): Promise<void> {
  const actorId = actor.id;
  if (!actorId) return; // Something went very wrong.
  const updates = breakNotifications[actorId] ?? { actor, userId, items: [] };
  updates.items.push(item);
  breakNotifications[actorId] ??= updates;
  debounceNotifyItemBreakage(actorId);
};

/**
 * Inner item break notification function, which takes an actor's entry from a Record of cached
 * item breakages, and either creates or appends to a message displaying which items were broken.
 *
 * @param actorId - The ID of an actor, used to index a cache record
 */
const _notifyItemBreakage = async (actorId: string) => {
  // Most recent message in chatlog
  const lastMessage = getGame().messages?.contents.pop();
  const brokenItems = breakNotifications[actorId];
  const { actor, userId } = brokenItems;
  // Copy array, then clean cached items when this function runs, regardless of GM or user state
  const items = breakNotifications[actorId].items.slice();
  breakNotifications[actorId].items = [];
  const speaker = ChatMessage.getSpeaker({ actor });
  const template = "/systems/nechronica/templates/chat/part-breakage.hbs";
  let message;

  // Evaluate if most recent message contains this actor's item breakage
  if (
    lastMessage?.data.speaker?.actor === speaker?.actor &&
    (lastMessage?.data.flags?.nechronica as { brokenItems: { name: string; id: string }[] })
      ?.brokenItems?.length
  ) {
    message = lastMessage;
  }

  if (getUser().id === getGame().users?.find((u) => u.isGM && u.active)?.id && message) {
    // Handle most recent message containing this actor's item breakage
    let brokenItems = message.getFlag("nechronica", "brokenItems") as {
      name: string;
      id: string;
    }[];
    // Don't add unnecessarily
    const missingBreaks = items.filter(
      (newItem) => !brokenItems.find((item) => item.id == newItem.id)
    );
    if (missingBreaks.length) {
      brokenItems = brokenItems.concat(
        missingBreaks
          .filter((item) => item.name && item.id)
          .map((item) => ({ name: item.name as string, id: item.id as string }))
      );
      const content = await renderTemplate(template, { brokenItems });

      return message
        .update({
          content,
          "flags.nechronica.brokenItems": brokenItems,
        })
        .then(() => {
          // @ts-expect-error Private declaration can safely be ignored
          ui.chat.scrollBottom();
        });
    }
  } else if (getUser().id === userId && !message) {
    // Handle creating a new message for actor's item breakage
    const brokenItems = items.map((item) => ({ name: item.name, id: item.id }));
    const content = await renderTemplate(template, { brokenItems });
    return ChatMessage.create({
      sound: null,
      speaker: ChatMessage.getSpeaker({ actor }),
      content: content,
      "flags.nechronica.brokenItems": brokenItems,
    }).then(() => {
      // @ts-expect-error Private declaration can safely be ignored
      ui.chat.scrollBottom();
    });
  }
};

/**
 * A debounced call of {@link _notifyItemBreakage}, allowing for update collection/caching.
 */
const debounceNotifyItemBreakage = debounce(_notifyItemBreakage, 200);

/**
 * Activates chat listeners on chat log
 */
export const activateListeners = function (html: JQuery<HTMLElement>): void {
  html.on("click", ".item-name", (ev) => toggleDescriptionDisplay(ev));
};

/**
 * Toggles display of item descriptions
 *
 * @param event - Triggering click event
 */
const toggleDescriptionDisplay = function (event: JQuery.ClickEvent<HTMLElement>) {
  event.preventDefault();
  const card = event.currentTarget?.closest(".chat-card");
  const description = $(card).find(".description");
  if (!$(description).text().length) return;
  if (description.hasClass("hidden")) {
    description.removeClass("hidden");
    description.hide();
    description.slideDown(150);
  } else {
    description.slideUp(150, () => description.addClass("hidden"));
  }

  // Update chat popout size
  const popout = card.closest(".chat-popout");
  if (popout) {
    popout.style.height = "auto";
  }
};
