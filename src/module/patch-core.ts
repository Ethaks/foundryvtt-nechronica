/* eslint-disable camelcase */

import { ItemDataSource } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/itemData";
import { DocumentType } from "@league-of-foundry-developers/foundry-vtt-types/src/types/helperTypes";

/**
 * Apply core Foundry patches
 */
export const initPatches = function (): void {
  // Patch tokens to redraw their bars if items were updated
  // @ts-expect-error Patching requires accessing provate methdo
  const Token__onUpdateBarAttributes = Token.prototype._onUpdateBarAttributes;
  // @ts-expect-error Patching requires accessing provate methdo
  Token.prototype._onUpdateBarAttributes = function (updateData) {
    Token__onUpdateBarAttributes.call(this, updateData);
    if (updateData?.items) this.drawBars();
  };

  const ItemDirectory__render = ItemDirectory.prototype.render;
  // @ts-expect-error Does exist, types missing
  ItemDirectory.prototype.render = function (
    force: boolean,
    context: Partial<SidebarTabRenderContext> = {}
  ) {
    const { action, data, documentType } = context;
    if (
      action === "update" &&
      documentType === "Item" &&
      data?.some((d) => d.data && "location" in d.data && d.data.location)
    ) {
      this.initialize();
      SidebarTab.prototype.render.call(this, force, context);
      return this;
    } else {
      return ItemDirectory__render.call(this);
    }
  };
};

interface SidebarTabRenderContext {
  action: "update" | string;
  data: DeepPartial<ItemDataSource>[];
  documentType: DocumentType;
}
