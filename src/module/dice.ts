import { ChatSpeakerData } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/chatSpeakerData";
import NechronicaActor from "./actor/actor-nech";
import { NechronicaRollData } from "./common/common-data";
import { getGame, getUser, localize } from "./lib/utils";

export default class NechronicaDice {
  /**
   * Standard helepr to manage Nechronica d10 rolls
   *
   * @param actor - The actor triggering the roll
   * @param dice - The number of d10s to be rolled
   * @param parts - Pre-defined parts like bonuses
   * @param template - Template to be used for the chat message
   * @param title - Title for the chat message
   * @param flavor - Flavor text
   * @param type - The type of roll shown next to formula
   * @param data - Roll data
   * @param image - Image shown in the message header
   * @param speaker - The chat message speaker
   * @param noSound - Whether sounds are to be suppressed
   * @param skipDialog - Whether the dialog is to be skipped
   * @param rollBonus - Bonus to be added to the roll
   */
  static async itemRoll({
    actor = null,
    dice = 1,
    parts = [],
    template,
    title = "",
    type,
    data,
    image,
    speaker,
    noSound = false,
    skipDialog = false,
    rollBonus = 0,
  }: NechronicaRollCardData = {}): Promise<ChatMessage | null | undefined> {
    data = data ?? actor?.getRollData() ?? {};
    let rollMode = getGame().settings.get("core", "rollMode");
    template = template || "systems/nechronica/templates/chat/item-card.hbs";
    skipDialog = skipDialog || (getGame().keyboard?.isDown("Shift") ?? false);
    let form;
    ChatMessage.getSpeaker();

    const templateData = {
      rollMode: rollMode,
      rollModes: CONFIG.Dice.rollModes,
      config: CONFIG.NECHRONICA,
      actor: actor,
      item: {
        name: title,
        img: image,
        data: {
          effect: "",
        },
      },
      result: { type: type, roll: "" },
      isRoll: true,
      roll: "",
    };

    if (!skipDialog) {
      const dialogContent = await renderTemplate(
        "systems/nechronica/templates/roll-dialog.hbs",
        templateData
      );

      form = await new Promise((resolve, _reject) => {
        new Dialog({
          title: title,
          content: dialogContent,
          buttons: {
            roll: {
              label: localize("NECH.Roll.Roll"),
              callback: (html) => resolve(html),
            },
          },
          default: "roll",
        }).render(true);
      }).catch();

      if (form === undefined) return;
    }
    form = form as JQuery<HTMLFormElement>;

    const nd = Number(form?.find('[name="dice"]').val() ?? dice);
    parts.push(`${nd}d10${nd > 1 ? "kh" : ""}`); // `

    // Roll bonus from form
    data.rollBonus = Number(form?.find('[name="roll-bonus"]').val()) + rollBonus;
    if (data.rollBonus) parts.push("@rollBonus");

    const roll = await Roll.create(parts.join("+"), data).evaluate({ async: true });
    if (roll.total == null) return;
    templateData.roll = await roll.render();

    // Add roll result and failure/success details
    if (roll.total <= 1) {
      templateData.result.roll = localize("NECH.Roll.Fumble");
    } else if (roll.total > 1 && roll.total <= 5) {
      templateData.result.roll = localize("NECH.Roll.Failure");
    } else if (roll.total >= 6 && roll.total < 11) {
      templateData.result.roll = localize("NECH.Roll.Success");
    } else if (roll.total >= 11) {
      templateData.result.roll = localize("NECH.Roll.Critical");
    }

    // Use rollMode from form, fall back to undefined to let ChatMessage handle rollMode
    rollMode = form?.find('[name="rollMode"]').val() as keyof CONFIG.Dice.RollModes;
    // Define sound if no Dice So Nice is enabled
    const sound = noSound ? undefined : getGame().dice3d?.enabled ? undefined : CONFIG.sounds.dice;

    // Handle chat message
    const chatData = {
      user: getUser().id,
      speaker: speaker ?? ChatMessage.getSpeaker({ ...(actor ? { actor } : undefined) }),
      rollMode: rollMode,
      type: CONST.CHAT_MESSAGE_TYPES.ROLL,
      roll: JSON.stringify(roll.toJSON()) ?? null,
      content: await renderTemplate(template, templateData),
      flags: { roll },
      sound,
    };
    const message = ChatMessage.create(chatData);

    // Return message promise for API usage
    return message;
  }
}

export interface NechronicaRollCardData {
  actor?: NechronicaActor | null;
  dice?: number;
  parts?: string[];
  template?: string;
  title?: string;
  flavor?: string;
  type?: string;
  data?: NechronicaRollData;
  image?: string;
  speaker?: ChatSpeakerData;
  noSound?: boolean;
  skipDialog?: boolean;
  rollBonus?: number;
}
