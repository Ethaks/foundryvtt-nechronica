/* -------------------------------------------- */
/*  Hotbar Macros                               */
/* -------------------------------------------- */

import NechronicaItem from "./item/item-nech";
import NechronicaActor from "./actor/actor-nech";
import { getGame, getUser, localize } from "./lib/utils";
import { ItemData } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/module.mjs";
import { DropData } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/foundry.js/clientDocumentMixin";
import { RollType } from "./config";

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 *
 * @param data - The dropped data
 * @param slot - The hotbar slot to use
 * @returns A Promise which resolves once the User update is complete
 */
export const createNechronicaMacro = async function (
  data: HotbarDropData,
  slot: number
): Promise<User | void> {
  if ("type" in data && data.type !== "Item") return;
  if (!("data" in data)) {
    return ui.notifications?.warn("You can only create macro buttons for owned Items");
  }
  const item = data.data as DeepPartial<ItemData>;

  let dialog;
  if (item.type === "bodyPart") {
    // Render dialog to determine macro type
    dialog = await new Promise((resolve, reject) => {
      new Dialog({
        title: localize("ChooseMacro.Title", { name: item.name }),
        content: localize("ChooseMacro.Content"),
        buttons: {
          message: {
            label: localize("Message"),
            callback: () => resolve(RollType.MESSAGE),
          },
          check: {
            label: localize("Check"),
            callback: () => resolve(RollType.CHECK),
          },
          attack: {
            label: localize("Attack"),
            callback: () => resolve(RollType.ATTACK),
          },
          combo: {
            label: localize("Combo"),
            callback: () => resolve(RollType.COMBO),
          },
        },
        close: () => reject(new Error("No type chosen")),
        default: "combo",
      }).render(true);
    });
    // The dialog was closed, no type was chosen
    if (dialog === undefined) return;
  }

  // Create the macro command
  const command =
    dialog === undefined || dialog === RollType.MESSAGE
      ? `game.nechronica.rollItemMacro("${item.name}");`
      : `game.nechronica.rollItemMacro("${item.name}", ${dialog});`;
  const macro =
    getGame().macros?.contents.find(
      (m: Macro) => m.name === item.name && m.data.command === command
    ) ??
    (await Macro.create({
      name: item.name ?? "",
      type: "script",
      img: item.img,
      command: command,
      flags: { "nechronica.itemMacro": true },
    }));
  if (!macro) return; // Somehow there's still now macro?
  return getUser().assignHotbarMacro(macro, slot);
};

/**
 * Roll an Item of a given name for the current actor.
 *
 * @param itemName - The item's name
 * @param rollType - The roll type
 * @returns The item roll's chat message (or nothing if no item was found)
 */
export const rollItemMacro = function (
  itemName: string,
  rollType: RollType = RollType.MESSAGE
): Promise<ChatMessage | void | null> | undefined | void {
  const speaker = ChatMessage.getSpeaker();
  let actor;
  if (speaker.token) actor = getGame().actors?.tokens[speaker.token];
  if (!actor && speaker.actor) actor = getGame().actors?.get(speaker.actor);

  // Get matching items
  const items = actor ? actor.items.filter((i) => i.name === itemName) : [];
  if (items.length > 1) {
    ui.notifications?.warn(
      localize("Warnings.MacroMultipleItems", {
        actorName: actor?.name,
        itemName,
      })
    );
  } else if (items.length === 0) {
    return ui.notifications?.warn(localize("Warnings.MacroNoItem", { itemName }));
  }
  const item = items[0] as NechronicaItem;

  if (rollType === RollType.COMBO) {
    rollType = RollType.MESSAGE;
    if (getGame().keyboard?.isDown("Alt")) rollType = RollType.CHECK;
    if (getGame().keyboard?.isDown("Control")) rollType = RollType.ATTACK;
  }

  // Trigger the item roll
  return item.roll({ rollType });
};

/**
 * Spend the controlled actor's AP. By default, increase the Spine Counter.
 * Optionally, just skip the actor's turn.
 *
 * @param options - Options to change the default behaviour
 */
export const spineCounterMacro = function (
  options: SpineCounterMacroOptions = {}
): Promise<void | { apSpent: number; usedSpineCounter: number }> | void | undefined {
  let { ap, free, skipTurn } = options;
  const speaker = ChatMessage.getSpeaker();
  let actor;
  if (speaker.token) actor = getGame().actors?.tokens[speaker.token] as NechronicaActor;
  if (!actor && speaker.actor) actor = getGame().actors?.get(speaker.actor) as NechronicaActor;

  // If free is not explicitly set, use Shift to enable it, otherwise default to false
  if (getGame().keyboard?.isDown("Shift")) free = !free;
  if (getGame().keyboard?.isDown("Control")) skipTurn = !skipTurn;

  if (!actor) {
    return ui.notifications?.warn(localize("Warnings.NoActorSelected"));
  }

  if (ap === "all") ap = actor.ap ?? 0;

  if (actor.ap && ap && actor.ap < ap && free === false) {
    return ui.notifications?.warn(
      localize("Warnings.MissingAPSpineCounter", {
        actorName: actor.name,
        ap,
      })
    );
  }

  if (ap == null) ap = 0;

  return actor.spendAp(ap, { toSpineCounter: !skipTurn, freeAP: free });
};

interface SpineCounterMacroOptions {
  /** How many AP to spend/gain */
  ap?: number | "all";
  /** Whether the actor's AP should be decreased */
  free?: boolean;
  /** Whether the Spine Counter should not be increased */
  skipTurn?: boolean;
}

// TODO: Check and improve type definition
type HotbarDropData =
  | ActorSheet.DropData.Item
  | DropData<Macro>
  | ({ type: string } & Partial<Record<string, unknown>>);
