/* eslint-disable @typescript-eslint/ban-types */
/**
 * Initialises general helper functions
 */
export const initHelpers = function (): void {
  if (!Array.prototype.deepEquals) {
    Object.defineProperty(Array.prototype, "deepEquals", {
      enumerable: false,
      writable: false,
      configurable: false,
      value: function (other: unknown): boolean {
        if (!(other instanceof Array) || other.length !== this.length) {
          return false;
        }
        /* eslint-disable-next-line */
        return this.every((v: unknown, i: number) => diffObject(other[i], v as object));
      },
    });
  }
};

export const addCircles = function (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data: Record<string, any>,
  circleMax = 0,
  valueAttribute = "value"
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Record<string, any> {
  data.circles = [];
  for (let i = 0; i < circleMax; i++) {
    data.circles.push({
      filled: i + 1 <= getProperty(data, valueAttribute),
    });
  }

  if (getProperty(data, "max")) {
    for (let i = 0; i < circleMax; i++) {
      data.circles[i].locked = i + 1 > getProperty(data, "max");
    }
  }

  if (data.permanent) {
    for (let i = 0; i < data.permanent; i++) {
      data.circles[i].permanent = true;
    }
  }

  return data;
};

/**
 * Deeply difference an object against some other, returning the update keys and values
 *
 * @param original  -   An object comparing data against which to compare.
 * @param other     -   An object containing potentially different data.
 * @param inner     -   Only recognize differences in other for keys which also exist in original.
 * @returns         -   An object of the data in other which differs from that in original.
 */
export const diffObjectDeep = function (
  original: object,
  other: object,
  { inner = false } = {}
): object {
  const _difference = function (v0: object, v1: object): [boolean, object] {
    const t0 = getType(v0);
    const t1 = getType(v1);
    if (t0 !== t1) return [true, v1];
    if (t0 === "Array") return [!(v0 as Array<unknown>).deepEquals(v1), v1];
    if (t0 === "Object") {
      if (isObjectEmpty(v0) !== isObjectEmpty(v1)) return [true, v1];
      const d = diffObjectDeep(v0, v1, { inner });
      return [!isObjectEmpty(d), d];
    }
    return [v0 !== v1, v1];
  };

  // Recursively call the _difference function
  return Object.keys(other).reduce((obj, key) => {
    if (inner && original[key as keyof typeof other] === undefined) return obj;
    const [isDifferent, difference] = _difference(
      original[key as keyof typeof other],
      other[key as keyof typeof other]
    );
    // @ts-expect-error Key is dynamically added; TODO: check types
    if (isDifferent) obj[key] = difference;
    return obj;
  }, {});
};

/**
 * Returns `game` after ensuring that it actually is initialised.
 */
export const getGame = (): Game => {
  if (!(game instanceof Game)) throw new Error("Game not initialised!");
  return game;
};

export const getUser = (): User => {
  const user = getGame().user;
  if (!(user instanceof User)) throw new Error("User not initialised!");
  return user;
};

/**
 * Regular Expression which localisation keys are checked against.
 */
const regex = /^ACTOR\.|ITEM\.|NECH\./;

/**
 * Returns a localised string from a localisation key.
 * If the key does not start with "ACTOR", "ITEM", or "NECH", a prefix of
 * "NECH" will be used for the key.
 *
 * @param key - The localisation key
 * @returns The localised string
 */
export const localize = (
  key: string,
  data?: Record<string, string | number | null | undefined>
): string => {
  if (regex.test(key)) return getGame().i18n.format(key, data);
  else return getGame().i18n.format(`NECH.${key}`, data);
};

/**
 * Returns a property of an object in string form,
 * providing a fallback in case no property is found.
 *
 * @param object - The object
 * @param key - The property's key
 * @param fallback - Optional fallback string
 * @returns The property or a fallback
 */
/* eslint-disable-next-line @typescript-eslint/ban-types */
export const getString = (object: object, key: string, fallback = "–"): string => {
  return foundry.utils.getProperty(object, key) ?? fallback;
};
