export const preloadHandlebarsTemplates = async function (): Promise<
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Handlebars.TemplateDelegate<any>[]
> {
  const templatePaths = [
    // Actor types
    "systems/nechronica/templates/actors/doll.hbs",
    "systems/nechronica/templates/actors/horror.hbs",
    "systems/nechronica/templates/actors/legion.hbs",
    "systems/nechronica/templates/actors/savant.hbs",
    "systems/nechronica/templates/actors/limited.hbs",

    // Actor parts
    "systems/nechronica/templates/actors/parts/parts-ol.hbs",
    "systems/nechronica/templates/actors/parts/parts-ul.hbs",
    "systems/nechronica/templates/actors/parts/biography.hbs",
    "systems/nechronica/templates/actors/parts/npc-header.hbs",
    "systems/nechronica/templates/actors/parts/ap-form.hbs",

    // Item types
    "systems/nechronica/templates/class.hbs",
    "systems/nechronica/templates/fetter.hbs",
    "systems/nechronica/templates/memory.hbs",
    "systems/nechronica/templates/bodyPart.hbs",

    // Chat templates
    "systems/nechronica/templates/chat/item-card.hbs",
    "systems/nechronica/templates/chat/part-breakage.hbs",

    // Tooltips
    "systems/nechronica/templates/tooltip-header.hbs",
    "systems/nechronica/templates/tooltip.hbs",

    // Roll Dialog
    "systems/nechronica/templates/roll-dialog.hbs",
  ];

  return loadTemplates(templatePaths);
};
