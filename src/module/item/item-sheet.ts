import { ActorType } from "../actor/actor-data";
import { addCircles } from "../lib/utils";
import { NECHRONICA } from "../config";
import { Modifier, NechronicaItemData } from "./item-data";
import NechronicaItem from "./item-nech";
import NechronicaActor from "../actor/actor-nech";

export interface NechronicaItemSheetData extends Omit<ItemSheet.Data<ItemSheet.Options>, "item"> {
  config: typeof NECHRONICA;
  actor: NechronicaActor;
  hasActor: boolean;
  actorType: ActorType | null;
  disableType: boolean;
  modifierGlobals?: {
    targets: typeof NECHRONICA.attackTypes;
    types: typeof NECHRONICA.modifierTypes;
  };
  item: NechronicaItemData;

  locations?: typeof NECHRONICA["locations"];
  timings?: typeof NECHRONICA["timings"];
  attackTypes?: typeof NECHRONICA["attackTypes"];
  partTypes?: typeof NECHRONICA["partTypes"];
}

export default class NechronicaItemSheet extends ItemSheet<
  ItemSheet.Options,
  NechronicaItemSheetData
> {
  /** @override */
  static get defaultOptions(): ItemSheet.Options {
    return mergeObject(super.defaultOptions, {
      width: 580,
      height: 620,
      classes: ["nech", "sheet", "item"],
      tabs: [
        {
          navSelector: ".sheet-tabs",
          contentSelector: ".sheet-body",
          initial: "details",
        },
      ],
    });
  }

  /** @override */
  get template(): string {
    return `systems/nechronica/templates/${this.item.data.type}.hbs`;
  }

  /** @override */
  async getData(): Promise<NechronicaItemSheetData> {
    const context = await super.getData();

    // TODO: Check replacement with regular CONFIG usage
    const configEntries =
      context.item.type === "bodyPart"
        ? Object.fromEntries(
            (["locations", "timings", "attackTypes", "partTypes"] as const).map((t) => [
              t,
              foundry.utils.deepClone(NECHRONICA[t]),
            ])
          )
        : null;

    // Prepare modifiers
    const modifierGlobals =
      "modifiers" in context.data.data && context.data.data.modifiers
        ? {
            targets: NECHRONICA.attackTypes,
            types: NECHRONICA.modifierTypes,
          }
        : undefined;

    const data = {
      cssClass: context.cssClass,
      document: context.document,
      limited: context.limited,
      owner: context.owner,
      title: context.title,
      data: context.data.data,
      item: context.item,
      itemData: context.data,
      editable: context.editable,
      options: context.options,
      config: NECHRONICA,
      actor: this.object.actor,
      hasActor: !!this.object.actor,
      actorType: this.object.actor?.data.type != null ? this.object.actor.data.type : null,
      disableType: !!this.object.actor && this.object.actor?.data.type !== "doll",
      modifierGlobals,
      ...(configEntries ?? {}),
    };

    if (data.item.type === "fetter") {
      addCircles(data, 4, "data.madness");
    }

    // @ts-expect-error Data types unnecessarily restrict the shape
    return data;
  }

  /** @override */
  activateListeners(html: JQuery<HTMLElement>): void {
    super.activateListeners(html);

    // Profile listener for body part sheets
    html.find("img.profile.part").on("click", (ev) => this._onProfileImgClick(ev));

    // Circle listener
    html.find(".circles").on("click", (ev) => this._onCircleClick(ev));

    // Modifier control listener
    html.find(".modifier-control").on("click", (ev) => this._onModifierControl(ev));
  }

  async _updateObject(
    event: Event,
    formData: FormDataExtended
  ): Promise<NechronicaItem | undefined | unknown> {
    const modifiers = Object.entries(formData).filter((e) => e[0].startsWith("data.modifiers"));
    // @ts-expect-error This mirrors the structure expected by Foundry
    formData["data.modifiers"] = modifiers.reduce((mods: Partial<Modifier>[], entry) => {
      const [index, field] = entry[0].split(".").slice(2) as [number, keyof Modifier];
      if (mods[index] == null) mods[index] = {};
      mods[index][field] = entry[1];
      return mods;
    }, []);

    return super._updateObject(event, formData);
  }

  /**
   * Handle clicks on the profile image.
   * This requires a non-default listener, since the image has no data-edit attribute set,
   * so that updating an item from a sheet does not by default overwrite the image with the
   * location specific fall icon.
   */
  _onProfileImgClick(ev: JQuery.ClickEvent<HTMLElement>): Promise<FilePicker.BrowseResult> {
    ev.preventDefault();

    const current = this.item.img;
    const fp = new FilePicker({
      type: "image",
      current: current ?? undefined,
      callback: async (path) => {
        await this.item.update({ img: path });
      },
      top: (this.position.top ?? 0) + 40,
      left: (this.position.left ?? 0) + 10,
    });
    // @ts-expect-error Instance works without target
    return fp.browse();
  }

  /**
   * Handle requests to add modifiers
   *
   * @param event - The triggering click event
   * */
  async _onModifierControl(event: JQuery.ClickEvent): Promise<void> {
    event.preventDefault();
    const a = event.currentTarget;
    if (!("modifiers" in this.item.data.data)) return;

    // Add new conditional
    if (a.classList.contains("add")) {
      await this._onSubmit(event as unknown as Event); // Submit any unsaved changes
      const modifiers = this.item.data.data.modifiers || [];
      this.item.update({
        "data.modifiers": modifiers.concat([NechronicaItem.defaultModifier]),
      });
    }

    // Remove a conditional
    if (a.classList.contains("delete")) {
      await this._onSubmit(event as unknown as Event); // Submit any unsaved changes
      const li = a.closest(".modifier");
      const modifiers = duplicate(this.item.data.data.modifiers);
      modifiers.splice(Number(li.dataset.modifier), 1);
      this.item.update({ "data.modifiers": modifiers });
    }
  }

  /**
   * Handle requests involving circles to set a numeric value with a defined maximum
   *
   * @param event - The triggering click event
   */
  _onCircleClick(event: JQuery.ClickEvent): void {
    const data = duplicate(this.item.data);
    const index = Number($(event.currentTarget).attr("data-index"));
    let target = $(event.currentTarget).parents(".circle-row").attr("data-target");
    const actor = this.actor;
    if (target === "item" && !!actor) {
      const itemData = duplicate(
        actor.items.find(
          (i) => i.id === $(event.currentTarget).parents(".item").attr("data-item-id")
        )
      );
      target = $(event.currentTarget).parents(".circle-row").attr("data-item-target");
      if (target == null) return;
      const value = getProperty(itemData, target);
      if (value === index + 1) {
        // If the last one was clicked, decrease by 1
        setProperty(itemData, target, index);
      } // Otherwise, value = index clicked
      else setProperty(itemData, target, index + 1);
      actor.updateEmbeddedDocuments("Item", [itemData]);
    }
    if (target == null) return;
    const value = getProperty(data, target);
    if (value === index + 1) {
      // If the last one was clicked, decrease by 1
      setProperty(data, target, index);
    } // Otherwise, value = index clicked
    else setProperty(data, target, index + 1);
    this.item.update(data);
  }
}
