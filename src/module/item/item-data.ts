import { NECHRONICA } from "../config";

export type ItemType = keyof typeof NECHRONICA.itemTypes;

export type NechronicaItemData =
  | NechronicaBodyPartData
  | NechronicaMemoryData
  | NechronicaFetterData
  | NechronicaClassData
  | NechronicaPositionData;

export interface NechronicaItemDataHelper<D, T extends ItemType> {
  type: T;
  data: D;
}

export type NechronicaBodyPartData = NechronicaItemDataHelper<NechronicaPartDataData, "bodyPart">;
type NechronicaMemoryData = NechronicaItemDataHelper<NechronicaMemoryDataData, "memory">;
export type NechronicaFetterData = NechronicaItemDataHelper<NechronicaFetterDataData, "fetter">;
type NechronicaClassData = NechronicaItemDataHelper<NechronicaClassDataData, "class">;
type NechronicaPositionData = NechronicaItemDataHelper<NechronicaPositionDataData, "position">;

interface NechronicaItemDataDataBase {
  description: string;
  effect: string;
}

export type Location = keyof typeof NECHRONICA.locations;

type Timing = keyof typeof NECHRONICA.timings;

type Attack = keyof typeof NECHRONICA.attackTypes;

export type PartType = "bodyPart" | "skill";

export interface Modifier {
  formula: string;
  optional: number | `${number}`; // For legacy compatibility
  target: Attack | "";
  type: keyof typeof NECHRONICA.modifierTypes | "";
  tag: string;
}

export type PartEffect = keyof typeof NECHRONICA.effectTypes;

export interface NechronicaPartDataData extends NechronicaItemDataDataBase {
  timing: Timing;
  cost: number;
  range: string;
  attack: Attack;
  threat: number;
  apBonus: number;
  used: boolean;
  type: PartType;
  damage: number;
  rollModifier: number;
  checkModifier: number;
  modifiers: Modifier[];
  effects: Record<PartEffect, boolean>;
  location: Location;
  broken: boolean;
}

interface NechronicaClassDataData extends NechronicaItemDataDataBase {
  armament: number;
  mutation: number;
  enhancement: number;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface NechronicaPositionDataData extends NechronicaItemDataDataBase {}

interface NechronicaMemoryDataData extends NechronicaItemDataDataBase {
  number: number;
}

interface NechronicaFetterDataData extends NechronicaItemDataDataBase {
  target: string;
  type: string;
  madness: number;
}

type NechronicaItemRollDataHelper<T extends NechronicaItemData["data"], U extends ItemType> = {
  itemType: U;
  item: T;
};

type NechronicaPartRollData = NechronicaItemRollDataHelper<
  NechronicaBodyPartData["data"],
  "bodyPart"
>;
type NechronicaMemoryRollData = NechronicaItemRollDataHelper<
  NechronicaMemoryData["data"],
  "memory"
>;
type NechronicaFetterRollData = NechronicaItemRollDataHelper<
  NechronicaFetterData["data"],
  "fetter"
>;
type NechronicaClassRollData = NechronicaItemRollDataHelper<NechronicaClassData["data"], "class">;
type NechronicaPositionRollData = NechronicaItemRollDataHelper<
  NechronicaPositionData["data"],
  "position"
>;

export type NechronicaItemRollData =
  | NechronicaPartRollData
  | NechronicaMemoryRollData
  | NechronicaFetterRollData
  | NechronicaClassRollData
  | NechronicaPositionRollData;
