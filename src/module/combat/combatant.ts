import { CombatantTurnHistoryEntry, SpendApOptions } from "./combat";

export class NechronicaCombatant extends Combatant {
  /*
   * Spend a number of AP for this combatant
   *
   * @param ap - Number of AP to be spent
   * @param options - Additional options affecting the spending behaviour
   */
  async spendAp(
    ap: number,
    options: SpendApOptions = {}
  ): Promise<{ apSpent: number; usedSpineCounter: number }> {
    options = mergeObject(
      { toSpineCounter: false, useSpineCounter: false, freeAP: false },
      options
    );
    if (options.useSpineCounter && options.toSpineCounter) {
      throw new Error("Cannot save to and use up Spine Counter at the same time");
    }

    // These errors should never be thrown, but the types makes them necessary
    if (!this.id) throw new Error("Combatant lacks ID");
    if (!this.parent) throw new Error("Combatant lacks parent Combat");

    // Save current state to history
    const combatHistory = this.parent.getHistory();
    const newHistoryEntry: CombatantTurnHistoryEntry = {
      index: combatHistory.lastIndex + 1,
      combatantId: this.id,
      initiative: this.initiative ?? 0,
      spineCounter: this.spineCounter,
    };
    const combatantHistory = this.data.flags.nechronica?.history?.slice() ?? [];
    combatantHistory.push(newHistoryEntry);

    // Reduce AP cost by spine counter up to its capacity
    let spineCounter = this.spineCounter;
    const apCost = options.freeAP
      ? 0
      : ap - (options.useSpineCounter ? Math.min(ap, spineCounter) : 0);
    // Adjust spine counter according to options
    if (options.useSpineCounter) spineCounter -= ap - apCost;
    if (options.toSpineCounter) spineCounter += ap;

    // Adjust initiative after spending AP
    const newInitiative = (this.initiative ?? 0) - apCost;
    await this.update({
      initiative: newInitiative,
      "flags.nechronica.spineCounter": spineCounter,
      "flags.nechronica.history": combatantHistory,
    });

    this.parent?.nextTurn(false);

    return {
      apSpent: apCost,
      usedSpineCounter: this.spineCounter - spineCounter,
    };
  }

  get spineCounter(): number {
    return this.data.flags.nechronica?.spineCounter ?? 0;
  }

  override _getInitiativeFormula(): string {
    // Get base formula
    let baseFormula = super._getInitiativeFormula();
    const penalty = this.data.flags.nechronica?.initiativePenalty;

    // Apply initiative penalty due to overspent AP from last round
    if (penalty !== undefined && penalty < 0) {
      baseFormula += ` + ${penalty}`;
    }

    return baseFormula;
  }
}
