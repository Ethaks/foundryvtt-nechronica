export default class NechronicaCombatantConfig extends CombatantConfig {
  /** @override */
  get template(): string {
    return "/systems/nechronica/templates/combat/combatant-config.hbs";
  }
}
