/**
 * The Nechronica specific Combat class, providing the initiative handling necessary
 * for the AP based turn order of actors.
 */
export default class NechronicaCombat extends Combat {
  /** @override */
  override async rollInitiative(ids: string[], options = {}): Promise<this> {
    await super.rollInitiative(ids, options);
    await this.update({ turn: 0 });
    return this;
  }

  /** @override */
  override async startCombat(): Promise<this | undefined> {
    // Setup turns to ensure combatant sorting
    this.setupTurns();

    // Initialise turn history
    await this.setFlag("nechronica", "roundHistory", []);

    return super.startCombat();
  }

  /** @override */
  override async nextTurn(cycle = true): Promise<this | undefined> {
    // Trigger next round if no combatant has more than 0 AP
    if (!this.combatants.some((c) => !!c.initiative && c.initiative > 0)) {
      return this.nextRound();
    }

    // Get current AP counter and actor type
    const initiative = this.combatant?.initiative;
    const curType = this.combatant?.actor?.data.type === "doll" ? "pc" : "npc";

    if (this.turn === null) return;

    // Determine next turn number
    let next = 0;
    // TODO: There has to be a better way than changing the function signature
    if (cycle) {
      for (const [i, turn] of this.turns.entries()) {
        const turnType = turn.actor?.data.type === "doll" ? "pc" : "npc";
        // Only cycle between same AP combatants
        if (turn.initiative !== initiative) continue;
        // Only cycle between actors of same type (PC/NPC)
        if (curType !== turnType) continue;
        // Only cycle forwards
        if (i <= this.turn) continue;
        next = i;
        break;
      }
    }

    // Either stay at highest AP, or cycle to next tied combatant
    return this.update({ turn: next });
  }

  /** @override */
  override async nextRound(): Promise<this | undefined> {
    // Get history and create a snapshot of all combatants so that this round can be restored
    const history = this.getHistory();
    await this.update({
      "flags.nechronica.roundHistory": this.data.flags.nechronica?.roundHistory?.concat([
        {
          index: history.lastIndex + 1,
          round: this.round,
          newRound: true,
          contents: this.combatants
            .filter((c) => !!c.data._id)
            .map((c) => ({
              combatantId: c.data._id ?? "",
              initiative: c.initiative ?? 0,
              spineCounter: c.spineCounter,
            })),
        },
      ]),
    });

    // Collect initiative penalties for actors with negative AP
    const updates = this.combatants.map((c) => {
      return {
        _id: c.id as string,
        "flags.nechronica.initiativePenalty": Math.min(c.initiative ?? 0, 0),
      };
    });
    await this.updateEmbeddedDocuments("Combatant", updates);

    await this.resetAll();

    this.rollAll();

    return this.update({ round: this.round + 1, turn: 0 });
  }

  /** @override */
  override async previousRound(): Promise<this | undefined> {
    const round = Math.max(this.round - 1, 0);

    if (round > 0) {
      // Get combat history, cut away entries from after last round's end
      const history = this.getHistory();
      const newRoundHistory = this.data.flags.nechronica?.roundHistory?.slice(0, -1) ?? [];
      const combatantUpdates = this.combatants.map((combatant) => {
        const previousState = history.previousRound?.contents.find(
          (e) => e.combatantId === combatant.id
        );
        const newCombatantHistory = combatant.data.flags.nechronica?.history?.filter(
          (e) => e.index < (history.previousRound?.index ?? 0)
        );
        return {
          _id: combatant.id,
          initiative: previousState?.initiative,
          "flags.nechronica.spineCounter": previousState?.spineCounter,
          "flags.nechronica.history": newCombatantHistory,
        };
      });
      await this.setFlag("nechronica", "roundHistory", newRoundHistory);
      await this.updateEmbeddedDocuments("Combatant", combatantUpdates);

      return this.update({ round: round, turn: 0 });
    }
    return this;
  }

  /** @override */
  override async previousTurn(): Promise<this> {
    const history = this.getHistory();
    if (history.lastEntry == null) return this;
    else if ("newRound" in history.lastEntry) await this.previousRound();
    else {
      await this.updateEmbeddedDocuments("Combatant", [
        {
          _id: history.lastEntry.combatantId,
          initiative: history.lastEntry.initiative,
          "flags.nechronica.spineCounter": history.lastEntry.spineCounter,
          "flags.nechronica.history":
            this.combatants
              .get(history.lastEntry.combatantId)
              ?.data.flags.nechronica?.history?.slice(0, -1) ?? [],
        },
      ]);
    }

    await this.update({ turn: 0 });
    return this;
  }

  getHistory(): CombatHistory {
    const turnHistory = this.combatants
      .filter((c) => !!c.data.flags.nechronica?.history)
      .flatMap((c) => c.data.flags.nechronica?.history ?? [])
      .sort((a, b) => a.index - b.index);
    const lastTurn = turnHistory[turnHistory.length - 1] ?? null;

    const roundHistory = this.data.flags.nechronica?.roundHistory ?? [];

    const completeHistory = [...turnHistory, ...roundHistory].sort((a, b) => a.index - b.index);
    const lastEntry = completeHistory[completeHistory.length - 1];
    const lastIndex = lastEntry?.index ?? 0;

    const reversedHistory = completeHistory.slice().reverse();
    // Necessary to determine which combatant history entries have to be deleted (all subsequent ones)
    const lastNewRoundIndex = reversedHistory.findIndex((e) => "newRound" in e && e.newRound);
    const previousRound =
      lastNewRoundIndex > -1
        ? (reversedHistory[lastNewRoundIndex] as CombatRoundHistoryEntry)
        : null;
    const currentRoundTurns =
      lastNewRoundIndex > -1
        ? (reversedHistory.slice(0, lastNewRoundIndex - 1).reverse() as CombatantTurnHistoryEntry[])
        : turnHistory;

    return {
      lastIndex,
      lastTurn,
      turnHistory,

      lastEntry,
      roundHistory,
      history: completeHistory,

      previousRound,
      currentRoundTurns,
    };
  }

  /* eslint-disable class-methods-use-this */
  /** @override */
  override _sortCombatants(a: Combatant, b: Combatant): number {
    // Get initiative values
    const initA = Number.isNumeric(a.initiative) ? a.initiative : -9999;
    const initB = Number.isNumeric(b.initiative) ? b.initiative : -9999;

    const initDifference = initB - initA;
    if (initDifference !== 0) {
      return initDifference;
    }

    const typeA = a.actor?.data.type;
    const typeB = b.actor?.data.type;

    if (typeA !== typeB) {
      // NPCs before PCs
      if (typeA === "doll") {
        return 1;
      } else if (typeB === "doll") {
        return -1;
      }
    }

    // Fallback ordering for tied NPCs
    if (a.data.tokenId != null && b.data.tokenId != null) {
      return parseInt(a.data.tokenId) - parseInt(b.data.tokenId);
    }
    return Math.random() - 0.5;
  }
}

export type CombatHistoryEntry = CombatRoundHistoryEntry | CombatantTurnHistoryEntry;

export type CombatRoundHistoryEntry = {
  index: number;
  newRound: boolean;
  round: number;
  contents: Omit<CombatantTurnHistoryEntry, "index">[];
};

export type CombatantTurnHistoryEntry = {
  index: number;
  combatantId: string;
  initiative: number;
  spineCounter?: number;
};

export interface CombatHistory {
  lastIndex: number;
  lastTurn: CombatantTurnHistoryEntry | null;
  turnHistory: CombatantTurnHistoryEntry[];
  lastEntry: CombatHistoryEntry;

  currentRoundTurns: CombatantTurnHistoryEntry[];
  previousRound: CombatRoundHistoryEntry | null;
  roundHistory: CombatRoundHistoryEntry[];

  history: CombatHistoryEntry[];
}

/**
 * Options adjusting if/how AP is spent, and whether the Spine Counter is affected
 */
export interface SpendApOptions {
  /**
   * Whether to use AP from the Spine Counter
   */
  useSpineCounter?: boolean;
  /**
   * Whether to collect AP in spine counter
   */
  toSpineCounter?: boolean;
  /**
   * Whether to subtract AP or not
   */
  freeAP?: boolean;
}
