/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */
const gulp = require("gulp");
const sass = require("@mr-hope/gulp-sass").sass;
const git = require("gulp-git");
const tagVersion = require("gulp-tag-version");
const replace = require("gulp-replace");
const sourcemaps = require("gulp-sourcemaps");
const rename = require("gulp-rename");
const del = require("del");
const fs = require("fs");

const gulpEsbuild = require("gulp-esbuild");
const esbuildSvelte = require("esbuild-svelte");
const sveltePreprocess = require("svelte-preprocess");

// BrowserSync
const browserSync = require("browser-sync");

/* ----------------------------------------- */
/*  Constants
/* ----------------------------------------- */
const SYSTEM_SASS = "./src/sass/**/*.scss";
const SYSTEM_MODULES = "./src/module/**/*.ts";
const SYSTEM_COMPONENTS = "./src/app/**/*.svelte";
const SYSTEM_FILES = [
  "./src/lang/**/*",
  "./src/icons/**/*",
  "./src/templates/**/*",
  "./src/system.json",
  "./src/template.json",
];
const RAW_FILES = ["./LICENSE", "./CREDITS.md"];
const DESTINATION = "./dist";

/* ----------------------------------------- */
/*  Build steps
/* ----------------------------------------- */

/**
 * Compiles SCSS files into CSS
 */
function compileSASS() {
  return gulp
    .src(SYSTEM_SASS)
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("./dist/styles/"))
    .pipe(browserSync.stream());
}

function moveSvelteCSS() {
  return gulp
    .src("./dist/nechronica.css")
    .pipe(rename("styles/components.css"))
    .pipe(gulp.dest(DESTINATION));
}

/**
 * Deletes the directory containing compiled files
 */
function deleteFiles() {
  return del(DESTINATION, { force: true });
}

/**
 * Copies all files not needing compilation into the destination directory
 */
function copyFiles() {
  return gulp.src(SYSTEM_FILES, { base: "./src" }).pipe(gulp.dest(DESTINATION));
}

/**
 * Copies raw files from the repository's root directory into the destination directory.
 */
function copyRaws() {
  return gulp.src(RAW_FILES).pipe(gulp.dest(DESTINATION));
}

/**
 * Copies the manifest from the destination directory into the root directory.
 * This shortens the manifest URL and ensures flexibility when dealing with the source file.
 */
function copyManifest() {
  return gulp.src("./dist/system.json").pipe(gulp.dest("./"));
}

/*
 * ESBuild for TypeScript compilation, development version
 */
function esbuildDev() {
  return gulp
    .src("./src/module/nechronica.ts")
    .pipe(
      gulpEsbuild({
        outfile: "nechronica.js",
        bundle: true,
        sourcemap: true,
        minify: false,
        format: "esm",
        platform: "browser",
        plugins: [
          esbuildSvelte({
            compileOptions: { dev: true },
            preprocess: sveltePreprocess(),
            cache: true,
          }),
        ],
      })
    )
    .pipe(gulp.dest(DESTINATION));
}

/*
 * ESBuild for TypeScript compilation, release version
 */
function esbuildProd() {
  return gulp
    .src("./src/module/nechronica.ts")
    .pipe(
      gulpEsbuild({
        outfile: "nechronica.js",
        bundle: true,
        sourcemap: true,
        minify: true,
        format: "esm",
        platform: "browser",
        keepNames: true,
        plugins: [esbuildSvelte({ preprocess: sveltePreprocess(), cache: true })],
      })
    )
    .pipe(gulp.dest(DESTINATION));
}

/* ----------------------------------------- */
/*  Watch and Reload tasks
/* ----------------------------------------- */

/**
 * Initialises browserSync
 */
function serve(done: () => unknown) {
  browserSync.init({
    server: false,
    proxy: {
      target: "localhost:30000",
      ws: true,
      proxyOptions: {
        changeOrigin: false,
      },
    },
    open: false,
    ghostMode: {
      clicks: false,
      forms: false,
      scroll: false,
      location: false,
    },
  });
  done();
}

/**
 * Reloads all browsers using the development server
 */
function reload(done: () => unknown) {
  browserSync.reload();
  done();
}

/**
 *  Retrieve current version
 */
function getTagVersion() {
  try {
    const file = fs.readFileSync("./src/system.json", "utf-8");
    const data = JSON.parse(file);
    return data.version;
  } catch (e) {
    console.log(e);
    return false;
  }
}

/**
 *  Increments the system's version
 */
function inc(importance: string) {
  const version = getTagVersion();
  if (version) {
    const oldVersion = version;
    let newVersion = version.split(".");
    switch (importance) {
      case "patch":
        newVersion[2]++;
        break;
      case "minor":
        newVersion[2] = 0;
        newVersion[1]++;
        break;
      case "major":
        newVersion[2] = 0;
        newVersion[1] = 0;
        newVersion[0]++;
        break;

      default:
        break;
    }
    newVersion = newVersion.join(".");
    return gulp
      .src(["./src/system.json"])
      .pipe(replace(oldVersion, newVersion))
      .pipe(gulp.dest("./src/"));
  } else {
    return gulp.src(["./src/system.json"]);
  }
}

/**
 *  Commit changes and make a new git tag
 */
function commitTag() {
  const version = getTagVersion();
  if (version) {
    return gulp
      .src(["./system.json"])
      .pipe(git.commit(`Release ${version}`))
      .pipe(tagVersion({ prefix: "" }));
  } else {
    return gulp.src(["./system.json"]);
  }
}

/**
 *  Watch updates
 */
function watchUpdates() {
  gulp.watch(SYSTEM_SASS, compileSASS);
  gulp.watch(SYSTEM_FILES, gulp.series(copyFiles, copyManifest));
  gulp.watch([SYSTEM_MODULES, SYSTEM_COMPONENTS], gulp.series(esbuildDev, moveSvelteCSS, reload));
  gulp.watch(`${DESTINATION}/nechronica.css`, moveSvelteCSS);
}

/* ----------------------------------------- */
/*  Export Tasks
/* ----------------------------------------- */

const defaultTask = gulp.series(
  deleteFiles,
  gulp.parallel(compileSASS, copyFiles, esbuildProd),
  moveSvelteCSS,
  copyManifest,
  serve,
  watchUpdates
);

const buildTask = gulp.series(
  deleteFiles,
  compileSASS,
  esbuildProd,
  moveSvelteCSS,
  copyFiles,
  copyManifest,
  copyRaws
);

exports.default = defaultTask;
exports.build = buildTask;
exports.watch = defaultTask;

exports.copy = gulp.series(copyFiles, copyManifest);
exports.css = compileSASS;

/**
 * Bumping version number and tagging the repository with it.
 * Please read http://semver.org/
 *
 * You can use the commands
 *
 *     gulp patch     # makes v0.1.0 → v0.1.1
 *     gulp feature   # makes v0.1.1 → v0.2.0
 *     gulp release   # makes v0.2.1 → v1.0.0
 *
 * To bump the version numbers accordingly after you did a patch,
 * introduced a feature or made a backwards-incompatible release.
 */
const patch = function () {
  return inc("patch");
};
const major = function () {
  return inc("major");
};
const minor = function () {
  return inc("minor");
};
exports.patch = gulp.series(patch, buildTask, commitTag);
exports.minor = gulp.series(minor, buildTask, commitTag);
exports.major = gulp.series(major, buildTask, commitTag);
