# Credits and Thanks

None of the people listed here necessarily endorse the project's author or the project itself.
Entries are listed in no particular order.

## Asacolips (Matt Smith)

For the scripts automatically updating versions and creating git tags, as well as the basis for the gitlab release workflow.

## Skoll's icons ([CC BY 3.0](http://creativecommons.org/licenses/by/3.0/))

- [Amputation](https://game-icons.net/1x1/skoll/amputation.html)

## Delapouite's icons ([CC BY 3.0](http://creativecommons.org/licenses/by/3.0/))

- [Hand bandage](https://game-icons.net/1x1/delapouite/hand-bandage.html)

## sbed's icons ([CC BY 3.0](http://creativecommons.org/licenses/by/3.0/))

- [Battery pack](https://game-icons.net/1x1/sbed/battery-pack.html)
- [Battery pack empty](https://game-icons.net/1x1/sbed/battery-pack-alt.html)
